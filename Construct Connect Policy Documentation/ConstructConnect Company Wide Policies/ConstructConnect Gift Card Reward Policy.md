title: GIFT CARD REWARD POLICY
author: Finance Operations
published: 2018-01-18

The purpose of this process implementation is to streamline the administrative activities supporting gift card incentive awards and harmonize the tax treatment for the benefit of all award recipients.


To achieve these objectives the tracking of gift card awards is being centralized in People & Culture and the ordering/fulfillment of gift cards in Finance.


Requestor will complete request form, obtain proper signatures for approval via electronic signature and submit form for tracking and fulfillment.


Amazon is our only vendor and ALL gift cards for any reason will be going through this process.


Gift card orders will be submitted to finance and processed on or about the 10th & 25th of each month. These are the only dates that gift card orders will be processed so please plan accordingly. No one-off cards in between orders.


Gift cards purchased individually and submitted as an expense will no longer be approved.


Gift cards are for internal use only – not for outside vendors, clients, etc.
