title: Employee Recognition and Special Sales Bonus (SPIFFs) Policy
author: Jeff Cryder - EVP
published: 2020-02-05

#### Policy Level
Very Important

#### Approver(s)
- Buck Brody – EVP Finance
- Jeff Cryder – EVP
- Jim Hill – EVP GM Trade Contractors
- Jon Kost – EVP GM General Contractors
- Howard Atkins – EVP GM Building Product Manufacturers

#### What location(s) and/or team(s) does the policy apply to?
All locations, All teams

#### Effective Date
2020-02-05

---

###### Policy Text

All requisitions made under the ReqLogic Item ID category _________________, AND directly result in the payment of special incentive compensation not otherwise covered by an approved compensation plan.  This category of requisition requires additional documentation (see Exhibit below) to support consideration and approval by appropriate parties (pursuant to ConstructConnect Delegation of Authority 3.05a):

- Requesting Manager / Team
- Date of Request
- Contest Period
- Financial Impact on Quarter / Year
- Business Purpose of Special Incentive
- Describe Eligible Participants
- Describe Payout Tiers
- Describe Contest & Payout Rules

This memo will be reviewed and approved by the following prior to contract signing:

- Team Manager / Director
- Member of Executive Leadership Team
- Senior Finance Representative

The preceding memo should be updated for relevant changes each time there is a new Contest.

All employee recognition bonuses and special sales bonus contests must be approved using this policy prescription prior to notification to eligible participants.

--- 

###### Exibit

#### Employee Recognition and Special Sales Bonus (SPIFFs) Approval Request

Team Name:

Requesting Manager:

Date of Request:

SPIF Contest Period:

Financial Impact:


Describe purpose of Special Incentive:
   


Describe eligible participants:
   


Describe payout award tiers:



Describe contest and payout rules:



###### Approvals:

SDescribe purpose of Special Incentive:
   

Describe eligible participants:
   

Describe payout award tiers:



Describe contest and payout rules:



Approvals:


Sales Team Manager / Director
- Signature
- Date

Executive Leadership Team Member
- Signature
- Date

Senior Finance Leader
- Signature
- Date 

