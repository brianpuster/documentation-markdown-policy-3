title: Account Reconciliation Policy
published: 2020-07-27
author: Kristina Meadows
# Account Reconciliation Policy

#### Policy Level
-	Very Important
	
#### Approvers
-	Dave Storer, Controller
-	Eric Bodge, Assistant Controller

#### Purpose
To establish guidelines for assessing, preparing and reviewing balance sheet account reconciliations on a consistent basis in accordance with US Generally Accepted Accounting Principles (“GAAP”) and Roper policy.

#### Policy

Account reconciliations are prepared and reviewed monthly for each active balance sheet account at its natural account level (stated as the first five digits in current GL string) in its respective functional currency.  All reconciliations should be prepared by business day 6 and reviewed by a separate individual by business day 7 with the exception of the following accounts that must be reconciled and reviewed by business day 4 at each quarter end (Cash, Accounts Receivable, Accounts Payable, Sales Tax Payable, Operating Accruals and Deferred Revenue)

##### Preparers responsibilities include:
     
- Comparison of ending GL balance to Independent Detail (Independent Detail must not be a listing of journal entries recorded to the account for the month)
- Identification and categorization of any difference in comparison above into distinct reconciling items
- Detailed description of any reconciling items that describe the nature of the item, how it gets resolved and when it gets resolved
- If identified reconciling items exceed $50,000 and should have been recorded in the month being reconciled, Controllership shall record a journal in that month or document why not recorded
- If identified reconciling items age greater than three months regardless of amount, Controllership shall record a journal in the third month or document why not recorded
- Unidentified differences between the GL and Independent Detail should be noted on a summary schedule that compares GL and independent detail for each account along with a note describing next action item to resolve.
- Any unidentified differences greater than $5,000 except for cash accounts must be identified by the next quarter end.
- Unidentified differences in Cash accounts must contain be researched and resolved the following month not to exceed business day 15
- Signoff of the account reconciliation acknowledging that each step was complete.  

##### Reviewer responsibilities include:
- Ensures account reconciliations are prepared in compliance with this policy.
- Review nature of the GL, independent detail and reconciling items for accuracy.


#### Completeness Check 
Once the period is officially closed Controllership will ensure all accounts are approved, reviewed and reconciled.