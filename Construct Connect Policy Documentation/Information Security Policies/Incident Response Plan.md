title: Incident Response Plan
author: VerSprite vCISO
published: 2020-06-09

#### Policy name:
Incident Response Plan

#### Policy level:
Annual General

#### Author:
VerSprite vCISO

#### Approver(s)
CTO (Bob Ven) and EVP Finance (Buck Brody)

#### Location or Team Applicability
All locations, all teams


#### Effective date:
2020-06-09

#### Policy text:

##### GENERAL

###### Objective
This document describes the overall plan for responding to Information Security Incidents at ConstructConnect. The goal of the Incident Response Plan is to detect and react to Security Incidents, determine their scope and risk, respond appropriately, and communicate the risk to stakeholders.


###### Scope
This document applies to all information and information systems at ConstructConnect and all employees who have access to information.


###### Responsibilities

**All Company Personnel** are responsible for disclosing potential Security Incidents via securityreviews@constructconnect.com as soon as they become aware of the issue. This is inclusive of emails, social media messages or other communications which are suspected to be social engineering (e.g., phishing).


**InfoSec Team** is responsible for:

- creating a Security Incident Response Team ("SIRT") to manage incidents;
- act as the primary point of contact for security concerns occurring across Company, inclusive of those which are identified as Security Incidents; and
- reviewing reports of security issues which includes:
    - establishing a Security Incident Response Team
    - auditing logs;
    - alert monitoring;
    - suspected phishing emails;
    - reports from ConstructConnect personnel; and
    - reports from third parties.

**Legal Department** is responsible for coordinating with InfoSec Team to declare whether an incident has occurred and initiate the full response duties in accordance with this policy to report privacy breach notices. A representative from the Legal Department will participate in the SIRT to advise on legal matters relating to evidence handling, breach notification or other related matters.

**Director of Communications** or designee is responsible for coordinating with the Legal Department upon request to develop internal employee communications and distribute breach notifications as instructed.

**Security Incident Response Team ("SIRT")** is responsible for minimizing the damage to or vulnerability of information resources throughout the lifecycle of a Security Incident. This includes investigation of the incident, mitigation of immediate risks involved, breach notification, documentation and conducting a post-mortem of the incident.

###### Review

This document must be reviewed for updates annually or whenever there is a material change in ConstructConnect's business practices that may reasonably implicate the security, confidentiality, integrity or availability of data and systems. The CIO owns this Policy, but any update or change must be approved by the Risk Committee.

##### DEFINITIONS

**Security Event** - any observable occurence that could potentially have information security implications. For example: 
- a change in the normal behavior of a given system, process, environment, workflow, or individual;
- a security monitoring alert triggers;
- an access request is denied; and
- receipt of a suspicious e-mail.

**Security Incident** - any violation or imminent threat of violation of computer security policies, acceptable use policies, or standard security practices, deliberately or inadvertently. For example:
- a user opens an attachment or link in a phishing e-mail;
- a user sends Confidential or Sensitive information to the wrong recipient; and
- a threat actor conducts a brute force, denial of service, or other attack against ConstructConnect's website or infrastructure.

**Data Breach** - a type of Security Incident where information obtained through unauthorized access or transmission of ConstructConnect data that is Confidental, Sensitive, or Internal.

**Impact** - the measure of how widely a security incident affects ConstructConnect.

**Urgency** - the measure of business criticality of the affected service(s).

##### DOCUMENTATION

Throughout the incident, the SIRT (or a designee on the team) must ensure that certain documentation of the investigation and its outcomes are captured for legal and retrospective review. The requirements of this documentation include:

- The initial identifying trigger (security alert, disclosure, etc.);
- Members of the SIRT;
- Triage activities taken along with a timeline of events;
- Remediation actions taken;
- Sensitive assets or Company resources impacted by the incident and follow-up steps (if any);
- Findings and/or results of the Post Mortem analysis.

Underlying evidence or documents created or captured throughout the incident process should be preserved during and after the incident, protected against deletion or tampering for a period of at least 6 years or other timeline per recommendations from the Legal Department related to the *Data Management Policy*.

##### INCIDENT RESPONSE

###### Initial Identification & Reporting

Security event reports can come from various sources, either by own observations (monitoring) or by notification from other sources. Security alerts will be regularly reviewed by the InfoSec Team. Automated security alerts should be reviewed by the InfoSec Team. Alerts which suggest that unauthorized access may have occurred, is occurring or is highly likely to occur given the circumstances should be reported to securityreviews@constructconnect.com with details about the issue and a note to escalate according to this policy.

###### Event Escalation and Triage

If a security alert, report from personnel or identified Security Event is determined to affect an information resource in a manner that qualifies as a Security Incident, the event should be escalated the InfoSec Team to a Security Incident with supporting justification and documented in an approved issue tracking system.

Triage involves the prioritization of response activities related to an escalated security incident or incidents.

###### Incident Classification

Upon a Security Incident being qualified, the criticality of the Security Incident can be classified using the following security and/or privacy considerations.

| **Priority** | **Security Impact** | **Privacy/Security**                                                       | **System / Office Impact**                                                | **# of Customers Impacted** |
| ------------ | ------------------- | -------------------------------------------------------------------------- | ------------------------------------------------------------------------- | --------------------------- |
| **Critical** | Major               | Confidential Information Disclosed                                         | Critical Product Infrastructure or Key Functionality of Production System | One or more Customers   |
| **High**     | Significant         | Confidential Information Disclosed                                         | Infrastructure or Minor Functionality of a Production System              | Customer Impact             |
| **Medium**   | Minor               | Internal/External Confidential Information improperly disclosed internally | Localized impact specific systems                                         | No Customer impact      |
| **Low**      | Little to no impact | None                                                                       | Any asset                                                                 | No Customer Impact      |

###### Response Team Formation

Immediately upon escalation of a Security Event to a Security Incident, the InfoSec Team shall establish an incident-specific Security Incident Response Team to rapidly investigate and respond to any Security Incident that is identified. The incident-specific SIRT includes both consistent team-members and ad-hoc members depending on the nature and scope of the Security Incident.

SIRT Consistent team members include:

- InfoSec Team;
- vCISO;
- VP of IT Operations;
- Legal Department; and
- Director of Communications or designee.

Upon escalation of a Security Event to a Security Incident, all consistent team members should immediately gather into a conference room onsite and/or video/tele-conference bridge involving any offsite team member(s). The consistent team members should then identify Ad-hoc team members and invite them to the conference room and/or teleconference bridge.

SIRT Ad-hoc team members may include:

- Human Resources representative;
- Finance representative; and/or
- Other team members from operations, technology or specific asset owners may be involved in the SIRT for a given incident, as appropriate, to reasonably ensure that maximum visibility into the incident is obtained for investigative and response purposes.
- Forensic Investigators. Forensic investigation and malware analysis skills are rare, highly sought after and expensive. Roper may consider contracting with a firm to provide support in the event of an incident. Due to possible legal proceedings or insurance claims associated with certain security events, engage with Roper legal before contracting a forensic third party. Note: Some of the insurance policies have recommended and/or required vendors (Roper legal maintains a list of insurance approved vendors).

From this SIRT, an incident manager will be selected and designated as the key decision maker within the SIRT for this incident. The Lead is responsible for ensuring the proper and timely communication to Roper.

Communicate with Roper's legal department and Director of Cybersecurity prior to proceeding with these steps:

- Engage internal and external Subject Matter Experts (SMEs), as needed.
- Contact external resources for assistance as appropriate.
- Control communications internally and with outside agencies and resources.
- Coordinate external notices of Security Incidents as may be required by law.

###### Incident Investigation Objectives

While the incident is ongoing, the SIRT should be guided by the following objectives:

- Preserve evidence and documents related to the investigation;
- Communicate within the SIRT, to the Steering Committee and to personnel as appropriate;
- Document all findings related to the investigation for later inclusion in post-incident reports;
- Identify the breadth and severity of the incident; and
- Find the root cause of the incident.

###### Determine Response Strategy

Under the direction of the incident manager, SIRT is to formally assume control and centralize information related to the Security Incident for analysis. Analysis of the information should identify which resources, both internal and external, are at risk and which harmful process are currently running on resources that have been identified as at risk.

SIRT is to determine which at risk resources require physical or logical remediation, including immediate removal or isolation, physically or logically.

When permissible, backups are to be conducted for the affected systems onto new, isolated media as this provides a critical snapshot of the system during its compromised state. This backup must be clearly marked for data retention purposes and to prevent use for any production level restore.

###### Incident Communication

The InfoSec Team and Legal Department (or other designated resource) are responsible for making sure that the SIRT prepares and updates reports as needed with regards to what was affected, the scope of the incident, response actions, and estimations for when business operations will resume to normal. Updates should also:

- Inform affected Company managers and Asset Owners as needed, depending on what teams are either directly or indirectly impacted by the incident;
- Inform senior management and the Steering Committee as relevant updates occur that influence any key external communication that may need to happen;
- Inform personnel only as needed (e.g. information about the incident if appropriate, instructions on what to do when contacted by media, restrictions on information sharing, etc.).
- The repository for the documentation, evidence and artifacts must be sent to Roper's Director of Cybersecurity for all Security Incidents and Data Breaches.

Discussing the presence of or any details around a security incident with external parties is strictly prohibited. This communication should only be handled through official channels as outlined in Breach Notification.

###### Breach Notification

The SIRT is responsible for identifying the type of data affected, the number of records, and the specific individual(s) impacted (if applicable) as a result of the incident. The SIRT will coordinate with Legal Counsel to determine what disclosures are legally required, including whether ConstructConnect must give notice to individuals, authorities and/or media.

**Notice to Individuals**

The SIRT and Legal Counsel should coordinate to determine whether individuals need to be notified about the incident and how ConstructConnect will provide notice. In general, notices should be sent by e-mail, if possible, or postal mail. Breach notice will consist of an email message featuring the official ConstructConnect logo, addressed to the individual at the last recorded email or physical address registered. Any notices returned as undeliverable should be re-sent via another channel if alternate contact information is available.

**Notice to Roper*

- The guidance of the Roper legal department should be sought if there is reason to believe that an incident may be a breach and/or may have legal ramifications; this includes: evidence collection, prosecution of a suspect or a lawsuit or if there may be a need for a memorandum of understanding (MOU) or other binding agreements involving liability limitations for information sharing. Roper's legal department will also assess whether any insurance carriers should be placed on notice. Legal representative should also review Roper's Insurance Claim Kit for further guidance.
- All significant Security Incidents and suspected/confirmed Data Breaches must be communicated to Roper's Director of Cybersecurity.
- All potential or confirmed Data Breaches must be communicated to Roper's legal department and Director of Cybersecurity within 12 hours.

**Notice to Internal Parties**

With assistance from Roper, other parties should be notified internally regarding the nature of the event. This might include external communication, internal communication to any employees, communication with external law enforcement, etc. Each area of interest may have a different response given the nature of the event and the information that needs to be communicated. When an incident is analyzed and prioritized, the SIRT needs to notify the appropriate individuals so that all who need to be involved will play their roles.

**Notice to Authorities**

Where required, notice to authorities must:

- Describe the nature of the breach including, where possible, the categories of data affected, the approximate or estimated number of records affected, the names and contact information of CISO and Legal Counsel at Company and a description of the likely consequences of the security incident based on the analysis performed thus far.
- Be performed within 72 hours following the confirmation that an incident occurred whenever feasible. Delays in notification must be documented.
- Roper's legal department must be consulted prior to external communications.

**Notice to the General Public**

Depending on the circumstances, a severe, widespread data breach, as determined by the SIRT and Legal Counsel, may require breach notification via ConstructConnect's website and/or major media outlets (e.g. including television, radio and print).

- Roper's legal department must be consulted prior to external communications.


**Notice Contents**

The content of the breach notice should include:

- A description of the incident in general terms; 
- A description of the type of CCPA or GDPR data that was the subject of the breach;
- A description of the general acts of Company;
- Steps taken to protect the information from further unauthorized access and/or acquisition;
- A telephone number that the individual may call for further information or assistance;
- Advice that directs the individual to remain vigilant by reviewing account statements and monitoring free credit reports, where applicable to the nature of the breach; and
- The timing of the notification.

##### POST MORTEM

In addition to the standard incident documentation process, the SIRT will be responsible for conducting an incident post-mortem. This post-mortem should utilize the template at [INSERT LINK] and is intended to analyze the root cause of the incident and the failure conditions that led to the incident being able to initially occur and then spread inside of ConstructConnect's environment.

The documented post-mortem report should be shared amongst technology and senior leadership stakeholders for review and discussion of remedial actions to prevent repeated issues.

##### TESTING

At least once a year, a mock incident will be initiated to facilitate testing of the current plan. The exact incident to be tested will be at the discretion of the InfoSec Team. The test may be formatted as a tabletop exercise.

All employees that could have an active role within incident response will be part of the test process.

Training regarding incident response responsibilities must be performed regularly to ensure employee's readiness for test and actual incidents.

##### REFERENCES

###### Relevant Regulations

- PCI-DSS
- US data privacy laws

###### Relevant NIST CSF Domain(s)/Category(ies)/Subcategory(ies)

- Respond

###### Relevant Roper CIS Domain(s)/Category(ies)/Subcategory(ies)

- 1.3, 16.1, 16.2

###### Related Policies, Plans, Procedures and Guidelines

- Written Information Security Program
- Data Management Policy
- Risk Management Policy

#### Insert any procedure or process documentation (optional):

[insert text]
