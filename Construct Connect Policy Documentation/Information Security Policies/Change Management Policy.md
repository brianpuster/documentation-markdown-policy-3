title: Change Management Policy
author: vCISO
published: 2020-09-03
effective date:
policy level: Annual General
approver(s): Dana Oney - SVP IT Ops., Bob Ven - CTO/CIO
location or team applicability: All locations, all teams
status: draft

# GENERAL

## Objective

The purpose of this Change Management Policy is to ensure that proposed changes are documented, reviewed, tested, approved, implemented, and released in a controlled manner and that the status of each change is audited and monitored. Non-technical changes (e.g., policy or procedure changes) are not subject to the requirements outlined in this policy.

## Scope

This policy applies to all ConstructConnect technology information, services, software, physical assets, and all individuals involved in changes to these areas. 

## Responsibilities

Risk Committee
: responsible for ensuring that the appropriate resource, process, people, and technology are deployed to enable this policy.

Corporate Information Security Office (CISO)
: responsible for auditing compliance with this policy in accordance with its guidelines.

Business Units
: responsible for providing representatives, as needed, to review change proposals. 

Technology Asset Owners
: responsible for following the guidelines of this policy, documenting proposals and presenting to CAB.

Change Advisory Board (CAB)
: responsible for championing the process and presentation of change proposals. 

## Review

This document must be reviewed for updates annually or whenever there is a material change in the Company's business practices that may reasonably implicate the security, confidentiality, integrity or availability of data and systems. The CISO owns this Policy, but any update or change must be approved by the Risk Committee.

# Change Type Definitions

This policy uses four different levels of change, which are defined below. Change types are largely driven by the possible risk to IT assets involved in the change. The individual or team responsible for delivering the change should make this determination using the framework below as a guide. 

Standard
: A typical change that has very little possible impact on ConstructConnect or its stakeholders. In this context, testing should be performed and approvals received, which can submitted in batches. Windows of execution or maintenance should be used where possible for changes not included in continuous software delivery models.

Normal
: Most software or system changes will fall into this type where they may have an impact on ConstructConnect stakeholders, data, or assets. In this context, testing and approvals are required. Ideally, changes should be deployed during maintenance windows if possible, unless part of normal continuous software deployment cycles. The change should include appropriate documentation and testing to ensure that it can be supported and troubleshooted once delivered. 

Emergency 
: A change that is required to remediate a business critical issue, but not one associated with an availability issue, this context may include security remediations. This change requires testing unless that testing will exacerbate the present emergency. Approvals and documentation can be obtained and constructed after the emergency is resolved.

Emergency Break-Fix
: A change that is required to remediate a major availability issue such as a business critical system crashing. In this context, the process of change management can be retroactively reviewed and a best effort attempt to ensure the fix will not cause further damage should be performed to the extent it is feasible. 

# Change Management Guidelines

The following guidelines must be followed to achieve policy purpose:

Framework
: A change management procedure based on the ITIL framework will be deployed to enable a standardized, documented, and auditable process that will ensure changes are not made without proper documentation, testing, appropriate approvals, and notifications. 

Supervision
: A Change Advisory Board (CAB) team will be assembled with a representative from each business unit, attending as is appropriate, to review and approve/revise/reject proposed changes and disseminate information as needed.

Risk-Centric Approach
: Changes to production information technology assets will be implemented according to predetermined criteria based on system criticality and risk level of change type.

Types of Change
: Changes are separated into 4 types (standard, normal, emergency, emergency break-fix) with a specific execution process for each type. The initiating team or individual should determine the type of change being made.

Governance Adherence
: Changes must adhere to security policies and guidelines. Changes may not violate or deviate from any other policies or procedures. Any deviation must be handled via a Risk Exception. 

Testing
: Changes are tested to the extent possible before deployment occurs to build confidence in the change.

Licensing
: Changes may not violate any licensing or use agreements.

Documentation
: Change requests are documented within the approved ITIL-based system and at a minimum will include requester, presenter, business unit, type, priority, business/system impact, security review, description, and approvals/rejections. Additional information may be needed depending on approval, type, and change target.

Approval Process
: Changes approved for implementation will include documentation showing approvers, testing plan that is relevant to use-case, execution plan, rollback plan, schedule and business acceptance contact. 

Maintenance/Execution Windows
: Execution of changes must be scheduled to minimize or eliminate disruption of infrastructure services, business unit, system operations or end users. 

Software Changes
: Application changes or new releases received from a vendor or internal developed code will follow the change process and will be vetted by the IT Team, manager(s) of affected Business Units and affected End User(s).

# Separation of Environments

With regards to software development and select IT systems, there must be a separate environment for development and testing from production. 

Non-operations team members who retain dual access to production and development/test (non-production) environments must be monitored when deploying changes to production. 

Segregation of Environments
: Software development access should be segregated from production environments including servers and databases. Operations teams should be the only roles with administrative access to the production environment. 

Separation of Duties
: Scheduled deployments should have different approvers and deployment personnel.

Deployment
: Automated and tool-based deployments should be developed, tested, and deployed within the same team with a tight feedback loop. Deployments should be managed and tracked via a centralized tool.

# Software Package Restrictions

The introduction of new open source software packages must be reviewed from a security, maintainability, and licensing standpoint before they can be introduced to an environment. 

All changes to software packages, including libraries and frameworks, supplied by vendors must ensure alignment with the Software Security Policy. 

Updates to any software package should be tested following the standard test and release process.

# Applications Review after Operating System Changes

Timeliness
: When an operating system needs an update or any sort of configuration change, the Operations Team must ensure that notification of the change is provided in time to allow for appropriate testing and reviews. 
Function Check

: Applications must be reviewed to ensure that they are up and running as expected after operating system changes.
Control Check

: Development teams must review application controls and integrity procedures to ensure that they have not been compromised by the operating system changes. 

# Outsourced Software Development

Where software development is outsourced, the following points must be considered:

- Agreements regarding licensing arrangements, and intellectual property rights must be in place with the Legal Department at ConstructConnect.
- All deliverables must go through the Release Management process to certify the quality and accuracy of the work.
- All deliverables must be tested before implementation for possible security issues (i.e., malicious code, vulnerabilities, etc.)

# REFERENCES

## Regulations.

- PCI-DSS

## NIST CSF

- Identify
- Protect

## Roper CIS Controls

- Cybersecurity Governance and Risk Management
- Access Management
- Configuration Management
- Segregation of Duties
- Supply Chain Risk Management

## Related Policies, Plans, Procedures and Guidelines

- Written Information Security Program
- Policies
    - Access Control Policy
    - Acceptable Use Policy
- Procedures
    - Configuration Management Procedure
    - Exception Management Procedure
