title: Personnel Security Policy  
author: vCISO  
published: 2020-09-03 
status: draft  
effective date:  
policy level: Annual General  
approver(s):   
location or team applicability: All locations, all teams  



# GENERAL

## Objective

The  purpose of this policy is to establish controls for the hiring, training, onboarding, and termination of personnel and enforces compliance with the Written Information Security Program and responsibilities relating to the contractual and regulatory requirements and the security of information assets. The Personnel Security Policy is intended to safeguard ConstructConnect workforce members as well as ConstructConnect information systems, assets, and data. 

## Scope

This policy applies to all ConstructConnect workforce members, inclusive of full-time employees, part-time employees, contractors, temporary workers, or others who have been granted access to ConstructConnect information systems. This policy also applies to any wholly-owned ConstructConnect subsidiaries.

## Responsibilities

Risk Committee
: Responsible for overseeing the reporting metrics produced around the security awareness and training program and provide recommendations as needed to the Corporate Information Security Office (CISO) on improvements to the program or metrics. 

Corporate Information Security Office (CISO)
: Responsible for:

    - Designing, managing, and monitoring security controls;
    - Auditing compliance with this policy in accordance with overall security requirements and best practices; and
    - Overseeing the proper functioning of the security awareness and training program in alignment with information security requirements and best practices.

Managers
: Responsible for ensuring that their direct reports participate in all assigned security awareness and training program initiatives. 

IT Team
: Responsible for implementing technical controls for training, background screening, and systems required to enforce and report upon this policy.

People and Culture
: Responsible for:

    - Management of the security awareness and training program; and
    - Monitoring the effectiveness of the training and awareness program
    - Reporting results and improvement suggestions to the Corporate Information Security Office (CISO)

## Review

This document must be reviewed for updates annually or whenever there is a material change in the Company's business practices that may reasonably implicate the security, confidentiality, integrity or availability of data and systems. The CISO owns this Policy, but any update or change must be approved by the Risk Committee.

# REQUIREMENTS

## Approval

Prior to the hiring process, a representative from People and Culture and the hiring manager must review the organization's needs and identify the most similar position within the organization. This position should be used as a reference for determining resource access needs which can be provided to the Information Technology and Security teams.

## Documentation

The role description should be created as a collaborative effort between the hiring manager and the People and Culture designee. This collaboration will combine the precise functional requirements, risk designation, and security posture provided by the hiring manager and the applicable employment laws and organizational requirements provided by People and Culture. This will produce the official role description containing a complete detail of the role requirements, required and/or desired qualifications, and the scope of responsibilities and authority. 

Execution of a new hire, role change, or termination must be fully documented by People and Culture using forms or a support request tracking system approved by ConstructConnect management. These forms or systems must include a checklist and templates for submitting requests to the Information Technology team and the CISO. 

## Pre-Employment Screening

Prior to ConstructConnect extending an offer for employment, all new hires and re-hires must undergo a pre-employment screening done by a non-biased third party. The background review for leadership, finance/accounting, or privileged IT access must include, at a minimum, identity, credit, and criminal checks. Additional screening may be required based on the role and risk designation. Any misleading, inaccurate, or false statements or assertions provided by an applicant is grounds for immediate disqualification for employment or discipline up to and including immediate termination upon discovery at any point. 

## Employment Agreement

All employment agreements must be fully executed prior to any business interactions between ConstructConnect and an individual. Offer letters should contain clauses  pertaining to confidentiality, non-compete, and non-solicitation in depth as is applicable to the role being filled. The new hire package must also include Code of Conduct, Monitoring (audio, video), and Handbook Acknowledgement documents. Additional documents may be required for contractors such as Proprietary Interest Protection Agreement (PIPA) and unique Vendor Data Security Addendums. 

## Orientation

Each new hire must complete orientation training within thirty (30) days of their start date, including but not limited to an overview of security policies and procedures, acceptable use, data management and any policies or procedures applicable to the individual role.

## Access Provisioning

When a ConstructConnect employee or other personnel joins (or a similar employment event such as a role change occurs), the Information Technology team should ensure that the individual is assigned the appropriate groups, permissions, applications, and data access. All access should be provided following the principles of least privilege and in alignment with the ConstructConnect Access Control Policy.

## Vendor, Temporary, and Contractor Accounts

Whenever a vendor, temp, or contractor is given access to any system, the account should be set to expire upon the termination of the contract or at the end of 90 days, whichever is sooner. Access may be extended upon proper requests and approvals only as necessary.

## Termination

Termination of an employment relationship should include immediate removal of access to infrastructure and applications including local, web based and/or hosted, revocation of any digital certificates and as is applicable, return of all ConstructConnect property including: facility/office/desk “metal” key(s); access/ID badge(s); token(s); smart card(s), parking permit, cell phone, computer, tablet, monitor, mouse, keyboard, dock and software license  information. Additionally, the IT Team must retain e-mail, files and employment records. Voluntary terminations providing a notice period must be reviewed by immediate management and based on type of access or risk associated to the role, management must either commence immediate release or allow a continuance of work during the period.

## Security Awareness and Training

On an annual basis, all employees should receive security awareness and training about ConstructConnect security policies that impact the individual in question. Awareness training and policy acknowledgement should convey that information security is to the benefit of the entire organization and all its employees, and that everyone is responsible for it. At the discretion of the CISO, security training can be inclusive of phishing tests administered by the Information Security department, 

# REFERENCES

## Regulations.

- US-Data Privacy Laws

## NIST CSF

* Identify
* Protect

## Roper CIS Controls

* Access Management
* Security Awareness

## Related Policies, Plans, Procedures and Guidelines

- Written Information Security Program

- Policies
    - Access Control Policy
    - Acceptable Use Policy

- Procedures
    - Onboarding Procedure
    - Change of Duties Procedure
    - Off-boarding/Termination Procedure
    - Access Extension Procedure

# Insert any procedure or process documentation (optional):
