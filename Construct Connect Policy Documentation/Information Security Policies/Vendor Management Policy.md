title: Vendor Management Policy
author: VerSprite vCISO
published: 2020-06-03

#### Policy name:
Vendor Management Policy

#### Policy level:
Annual General

#### Author:
VerSprite vCISO

#### Approver(s)
EVP Finance (Buck), Controller (Dave S.), CTO (Bob)

#### Location or Team Applicability
All locations, all teams

#### Effective date:
input date using YYYY-MM-DD format

#### Policy text:

##### GENERAL

###### Objective

The policy establishes guidelines for the Vendor Risk Management Process that ensures that risks arising from third parties are identified, managed, mitigated and reported in an organized and timely manner. This policy shall ensure that all agreements between ConstructConnect and third parties have acceptable level of Information Security Governance.

###### Scope

This policy applies to all employees, contractors, sub-contractors and vendors for ConstructConnect. This policy applies to all contracts and agreements between ConstructConnect and third parties.

###### Responsibilities

**Steering Committee** must:

- Ensure the appropriate resources, processes, people and technology are deployed to enable this policy;
- Review and update the Vendor Management Policy to ensure alignment with industry standards, legal and regulatory requirements;
- Receive and review Vendor Management reporting to ensure overall compliance;
- Ensure that appropriate personnel take action to remedy non-compliance with this policy;
- Review all requests to add high-risk Vendors and provide approval/rejection; and
- Create a Vendor Management Committee either directly selecting the committee members or appointing a specific individual to select the committee members.


**Vendor Management Committee** must:

- Maintain a central repository of all Vendor due diligence and supporting documentation;
- Implement the Vendor Management Policy;
- Produce and compile appropriate reporting for business review;
- Facilitate Vendor terminations with the Steering Committee and Legal to limit contractual liability;
- Review all requests to add Vendors;
 - Approval/rejection of low or medium risk Vendors;
 - Escalation of high-risk Vendors to steering committee for review; and
- Hold accountable all Company employees managing Vendor relationships and ensure compliance with this policy.

**Legal Department** must:

- Draft or review Statements of Work and Service Level Agreements;
- Ensure all contracts appropriately address privacy, confidentiality, integrity and availability requirements;
- Ensure all contracts appropriately address transfer of risk as needed;
- Validate that all contracts are signed by all necessary parties; and
- Maintain copies of all contracts between Company and Vendors.

**Personnel Directly Managing Vendor Relationships** must:

- Ensure concerns identified via diligence or ongoing monitoring are immediately escalated to the Vendor Management Committee for review;
- Conduct due diligence of Vendors prior to onboarding and submit supporting documentation to the Vendor Management Committee for approval;
- Perform ongoing monitoring of Vendors and ensure that Vendors comply with contract terms and service-level agreements;
- Ensure the Vendor addresses any concerns identified by Company via contract/SLA review or training within a reasonable period and, if not appropriately rectified, initiate termination of relationship;
- Maintain appropriate documentation throughout each Vendor's lifecycle; and
- Remedy Vendor compliance concerns via Vendor training or recommendation of relationship termination to Vendor Management.

###### Review

Company shall review this Vendor Management Policy and the security measures defined herein at least annually, or whenever there is a material change in Company's business practices that may reasonably implicate the security, confidentiality, integrity, or availability of records containing personal or other sensitive information. Company shall retain documentation regarding any such program review, including any identified gaps and action plans.


##### REQUIREMENTS

Before engaging any Vendor, that Vendor must be selected, reviewed, and approved.

- Vendor Selection must:
 - Comply with the requirements of this policy and the Vendor Request Form; and
 - Align to business strategy and meet business requirements.
- Vendor Review must include a Vendor Risk Analysis including risk rating, impact analysis, and due diligence.
- Vendor Approval can only be provided by:
 - the Vendor Management Committee for Low/Medium risk Vendors; and
 - the Steering Committee for High risk Vendors.

###### Risk Analysis

Risk analysis should be conducted for each potential Vendor and noted on the New Vendor Request Form. The Vendor Risk Rating will be based on the various factors guided by the following;

- **Low** Owned and based in the United States, little or no history of customer complaints or litigation, principals or company background is clear, few or no sub-contractors used, stable financial status, no access to PII or total contract amount less than $[xxxxx].
- **Moderate** Foreign owned with United States operations, minimal history of customer complaints or litigation, principals or company background is clear, few or no sub-contractors used, appears to have stable financial status, no access to PII or total contract amount less than $[xxxxx].
- **High** Large portion of operations located outside the United States, recent or significant history of customer complaints or litigation, principals or company background has significant legal infractions or is unknown, heavy use of sub-contractors needed to be able to provide services, financial status unknown, access to PII or total contract amount more than $[xxxxx].

###### Impact Analysis

Impact analysis should be conducted for each potential Vendor and noted on the New Vendor Request Form. The Vendor Impact Rating will be guided by the following;

- **Normal** Services provided by a Vendor without which Company would have minimal or no impact and would be able to conduct business.
- **Significant** Services provided by a Vendor without which Company would be impacted but would still be able to conduct business.
- **Critical** Services provided by a Vendor without which Company would not be able to conduct business.

###### Due Diligence

Company will conduct due diligence on each Vendor prior to entering a contractual relationship. The requirements will be based on the rating assigned to each Vendor. Focus of the due diligence must be relevant to the circumstance of each engagement and as such previous interactions must not be considered.

##### REPORTING

Vendor Management will provide quarterly reporting regarding the status of active Vendors to the Steering Committee, including but not limited to, reason for engagement, vendor champion, infractions, remediation, risk or impact change.

##### REFERENCES

###### Relevant Regulations.
- PCI-DSS

###### Relevant NIST CSF Domain(s)/Category(ies)/Subcategory(ies).
- Identify

###### Relevant Roper CIS Domain(s)/Category(ies)/Subcategory(ies).
- 1.3, 13.1, 13.2, 13.3


###### Related Policies, Plans, Procedures and Guidelines.
- Written Information Security Program
- Risk Management Policy

#### Insert any procedure or process documentation (optional):


[insert text]
