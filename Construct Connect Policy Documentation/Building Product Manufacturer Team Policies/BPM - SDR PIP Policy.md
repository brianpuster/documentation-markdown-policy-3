title: BPM - SDR PIP Policy
published: 2019-09-11
author: Howard Atkins - EVP & GM BPM

## Major Market Sales Development Representative

### Performance Improvement Plan Process -- Effective 5/1/2019

Sales Development Representatives' performance will be reviewed by Sales Management on a monthly basis at the beginning of each month for the previous month's key performance objectives against the individual's established minimum monthly threshold. If a Sales Development Representative does not meet their full threshold, the performance improvement plan process will begin as outlined below. Sales Development Representatives who have not yet completed the 90-day orientation period will not be subject to this process, but will be held responsible for a successful orientation period, which consists of:

### Orientation Period Performance Guidelines

-   Satisfactory attendance -- outlined in the Employment Handbook
-   Attitude that is open to coaching and feedback
-   Achieve a minimum of 60% of the appointment set ramp metric goal each month
-   Completion of all required Certifications

*If team members are unable to demonstrate their ability to achieve a satisfactory level of performance during their orientation period, their employment could be terminated.*

*Upon successful completion of the orientation period team members will enter the "regular" employment classification.*

#### Key Performance Objective to be Monitored

**Over 6 months in SDR role**

-   Calls: Average 50 calls minimum per day
-   Talk Time: Average 60 minutes talk time per day
-   Salesforce Activity: Average 50 activities minimum per day
-   Appointments Set: Minimum 24 appointments set per month
-   Prospecting: Minimum 8 new opportunities identified per month
-   Bookings: Minimum of 60% to quota Rolling 60-day achievement

**Less than 6 months in SDR role**

-   Calls: Average 50 calls minimum per day
-   Talk Time: Average 60 minutes of talk time per day
-   Salesforce Activity: Average 50 activities minimum per day
-   Appointments Set: Minimum 12 appointments set per month
-   Prospecting: Minimum 8 new opportunities identified per month
-   Bookings: Minimum of 50% to quota Rolling 60 achievement

**Performance Improvement Process A: (more than 6 months in role)**

-   Missed Bookings Goals below 60% of average quota for two consecutive months: 1<sup>st</sup> Stage Improvement Plan
-   Missed goal below 50% while on PIP can lead to termination.
-   Missed Bookings Goal below 60% for a rolling three-month period: 2<sup>nd</sup> Stage Improvement Plan.
-   While on 2<sup>nd</sup> Stage Improvement Plan you must hit minimum 65% for rolling three-month period. Failure to do so can result in termination.
-   Stages run consecutively.
-   In order to be removed from performance improvement plan you must maintain 65% of goal for two consecutive months.

**Performance Improvement Process B: (Less than 6 months in role)**

-   Missed Bookings Goals below 50% of average quota for two consecutive months: 1<sup>st</sup> Stage Improvement Plan
-   Missed Bookings goal below 40% while on PIP can lead to termination.
-   Missed Bookings Goal below 50% for a rolling three-month period: 2<sup>nd</sup> Stage Improvement Plan.
-   While on 2<sup>nd</sup> Stage Improvement Plan you must hit minimum 55% for rolling three-month period. Failure to do so can result in termination.
-   Stages run consecutively.
-   In order to be removed from performance improvement plan you must maintain 55% of goal ro for two consecutive months.

Management reserves the right to by-pass stages of the improvement process depending on the nature of the issue.

Minimum monthly threshhold as it pertains to the performance plan will be pro-rated for approved FMLA or bereavement absences.

SDRs with planned time off (PTO time) scheduled and approved at least a month in advance for a minimum of 3 consecutive days will only be responsible for the pro-rata portion of the goal associated with the number of business days worked. 

This is a guideline for a performance improvement plan and is subject to change at the discretion of the CRO or CPO of ConstructConnect.

I have read and acknowledge the Major Market Sales Development Representative Performance Improvement Plan Process to be the process I will be held accountable to regarding my performance.
