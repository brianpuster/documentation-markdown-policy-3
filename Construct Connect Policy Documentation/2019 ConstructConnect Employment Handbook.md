title: 2019 ConstructConnect Employment Handbook  
published: 2019-01-01  
author: Jennifer Manguiat  


# TEAM MEMBER EMPLOYMENT HANDBOOK ACKNOWLEDGEMENT FORM

The employment handbook describes important information about ConstructConnect. I understand that I am responsible for reading the handbook, familiarizing myself with its contents and adhering to all of the policies and procedures of ConstructConnect whether set forth in this handbook or elsewhere. I further understand that I may consult with People & Culture (HR) regarding any questions I may have about such policies and procedures.

Since the information, policies and benefits described here are necessarily subject to change, I acknowledge that revisions to the handbook may occur except to the policy of employment-at-will, which may only be modified by written agreement by the Chief Executive Officer (CEO) of ConstructConnect. All such changes will be communicated through official notices, and I understand that revised information may supersede, modify or eliminate existing policies.  

Furthermore, I acknowledge and understand that employment with ConstructConnect is not for a specified term and is at the mutual consent of the team member and the Company which is classified as "At-Will" employment. Accordingly, either I or ConstructConnect can terminate the relationship at will, with or without cause, at any time, so long as there is not a violation of applicable federal law or state law.

I acknowledge that this handbook is neither a contract of employment nor a legal document. I have received the handbook, and I understand that it is my responsibility to read, understand and comply with the policies contained in this handbook & state supplements and any revisions made to it.

The [Employment Handbook & State Supplements](https://theloop.constructconnect.com/Interact/Pages/Content/Document.aspx?id=6527&SearchId=) can be found on the ConstructConnect Intranet. If there are questions regarding accessing the Employment Handbook or State Supplements contact People & Culture (HR).

NOTE: This document is signed electronically. Contact People & Culture with any questions.

TEAM MEMBER'S NAME (printed):

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  

TEAM MEMBER'S SIGNATURE:

*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_* 

DATE: *\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_* 



# INTRODUCTION

> Team -
> 
> The last 365 days have been fast-paced, as we continue our transformation. It's required hard work, commitment and tremendous effort. Together, we have made even more progress toward our goals. And, I anticipate our most transformational days are in front of us.
> 
> If you're new to the team, we're so glad you've chosen to join us. I say this often and each time I mean it more and more - it's never been a better time to be part of ConstructConnect.
> 
> In order for us to continue to *transform together* into a new year, it's important that we understand the foundation of this transformation with our Success Equation:
> 
> ### Customer Success + Your Success = Our Success

> - When we remain focused on ensuring our customers achieve success, we achieve success also. When they win, we win.  
> - Together, we recognize that when our customers and team members succeed that ConstructConnect is able to be successful.  
> - You play a critically important role as we all work together to deliver for our customers.
> 
> Though everything we do is focused on the customer experience - ensuring our customers are more successful with us than without us - we believe your success is essential. One of the ways we prioritize this is by creating a culture focused on engagement that's built on our five values: Respect, People First, Empowerment, Collaboration, Drive.  
> 
> When we each make it a priority to live out our values day-in and day-out, we ensure everyone has the opportunity to achieve their potential and find professional success.  
> 
> Please take time to review the Handbook in detail. If you have any questions, reach out to one of our People & Culture managers.

> We are stronger together -

> **Dave Conway**  
> **President & CEO, ConstructConnect**

## Forward

This Employment Handbook has been developed to get you acquainted with our Company and answer many of your questions. ConstructConnect believes that it is important to keep you fully informed about its policies, procedures, practices and benefits, and what you can expect from the Company and the obligations you assume as a team member of the Company.  These practices are designed to provide consistent and equal treatment.  It is your responsibility to read, understand and comply with the policies contained in this handbook and any revisions made to it. If you have any questions on any matter pertaining to your employment, please contact your manager or the Chief People Officer.

This Employment Handbook is intended to provide members of the Company with basic information. An employer and team member can agree to establish a relationship of "At-Will" employment based upon allowing a team member's continuing employment based upon the team member providing a continuing service to an employer. Such an "At-Will" relationship has been created in your employment with the Company where both you and the employer may end the employment relationship with or without reason, cause or notice. This means that you have the right to leave at any time for any reason or for no reason at all and the Company has the right to separate you from employment at any time for any lawful reason or no reason at all.

The policies and practice statements, which are included in this Handbook, reflect a great deal of care and concern for the people who make it possible for ConstructConnect to exist...ITS TEAM MEMBERS. These policies may change with time or they may need to be clarified, amended, supplemented or revoked. The Company maintains the responsibility and the right to make these changes at any time with or without notice when this happens. The Chief Executive Officer (CEO) of ConstructConnect has authority to establish other policies, procedures, rules and benefits, which are deemed necessary to implement management decisions.

The descriptions of various fringe benefits such as group insurance are summaries only. Should the description in this Handbook differ with formal agreements or documents involved, the formal and complete plans are to be considered correct. As noted above, policies and practices are subject to change.  

Please consult with your manager or People & Culture (HR) if you have questions related to any of the information supplied in this Employment Handbook. The information in this Handbook replaces any of the same previously issued information.

## Company Vision

We will transform the way the construction industry does business by providing our customers the information needed to drive their success.

## Company Mission

We will connect the North American construction industry with the best information, technology, solutions and market relationships to drive our customers' and investors' success through a committed investment in the personal and professional growth of our talent.

## Company Values

Respect
: Embracing the talent and uniqueness of each person, we practice trust, transparency and integrity in our words, actions and commitment to one another and our customers.

People First
: Family, community and each individual's personal and professional well-being and growth is the foundation of our culture - profit follows.

Empowerment
: Trusting in and developing the abilities and ideas of
our people to create and maintain a culture in which the organization
continuously operates, innovates and flourishes.

Collaboration
: With humility, holding each other accountable to be passionate and engaged to be focused on our customers' success.

Drive
: Fearlessly and energetically pursuing excellence and results.

## Customer Relations Statement

Customers are among the Company's most valuable assets. Every team member represents the Company to our customers and the public. The way we do our jobs presents an image of our entire Company. Customers judge all of us by how they are treated during each team member interaction.  Therefore, one of our first business priorities is to assist any customer or potential customer. Nothing is more important than being courteous, friendly, helpful, and prompt in the attention you give to customers. Our personal contact with the public, our manners on the telephone, and the communications we send to customers are a reflection not only of ourselves, but also of the professionalism of the Company.  Positive customer relations not only enhance the public's perception or image of ConstructConnect., but also pays off in greater customer loyalty.

## Internal Communications

We value open, consistent, quality communications with our team members.  ConstructConnect's business is all about effective communications and we know firsthand how essential internal communication is to success. We aim to ensure that all team members understand our values, mission and strategy, as well as our products, services and the markets in which we do business.

All team members share the responsibility of staying informed so that we, as a company, can gain a competitive advantage. We encourage you to discuss with your managers ways to stay abreast of the rapidly changing business climate, including any resources you may need to excel in your position or to keep current on new company developments. Team members are also encouraged to contribute news or ideas to our company Intranet.  

## Open Door Initiative

At ConstructConnect, we believe that communication is the heart of good team member relations. Team Members should share their concerns, seek information, provide input and resolve work-related issues by discussing them with their managers until they are fully resolved.

The problem-solving procedure whereby management will seek answers for questions or problems a team member may have is the following.

1.  If any questions, concerns or difficulties arise, a team member is advised to discuss them with their immediate manager.
1.  If the issue cannot be satisfied by the manager, a team member is advised to bring it to the attention of the Team Director/Vice President.
1.  If the issue still has not been resolved or needs further clarification, the team member is advised to speak with a member of People & Culture (HR).  
1.  If the issue still has not been resolved by a member of People & Culture (HR), the team member is advised to speak with the Chief Executive Officer (CEO).  

(In certain circumstances a team member may contact People & Culture
(HR) and forgo Steps 1 & 2.)

Regardless of the situation, team members should be able to openly discuss any work-related problems and concerns without fear of retaliation.

If a team member has a concern about discrimination and/or harassment, the Company has set up special procedures to report and address those issues. The proper reporting procedures are set forth in the Company's Workplace Harassment Policy.

## Policy Exception Requests

Any team member can request an exception to a Company policy or procedure when circumstances indicate that proper implementation will not accomplish the Company intent. For more information or a policy exception review contact the Chief People Officer.

# EMPLOYMENT

## At-Will

Nothing contained in this Handbook or in any other materials or information distributed by the Company creates a contract of employment between an individual and ConstructConnect. Employment is on an at-will basis. This means that individuals are free to resign their employment at any time, for any or no reason, with or without notice, and ConstructConnect retains the same right. No statements to the contrary, written or oral made either before or during an individual's employment can change this. No individual, manager or other officer can make a contrary agreement, except the Chief Executive Officer (CEO), and even then, such an agreement must be set forth in a written employment contract with the individual, signed by the Chief Executive Officer (CEO).

## Employment Eligibility

ConstructConnect, as an Equal Opportunity Employer, is committed to employing individuals legally entitled to work in the United States without regard to citizenship, ethnic background or place of national origin.

Federal regulations require the Company to comply with the Immigration Reform and Control Act of 1986. All new team members must complete an I-9 form and provide proof of their identity and ability to work in this country. People & Culture (HR) is responsible for ensuring the completion of the I-9 form and verifying the eligibility to work in the United States. Team members will be expected to complete the I-9 form during their first three (3) days of work.  

Additionally, any team member younger than eighteen (18) years of age will be required to present proof of age and work permit as required by law.

## Orientation Period

All new, re-hired, and transferring team members work on an orientation period of 90 days following the date of hire or rehire date. ***Note: Some teams may have a longer orientation period to which they must adhere to.***

The orientation period is intended to give new team members the opportunity to demonstrate their ability to achieve a satisfactory level of performance and to determine whether the new position meets their expectations. ConstructConnect uses this period to evaluate team members' capabilities and overall performance.

If the Company determines that the orientation period does not allow sufficient time to thoroughly evaluate the team member's performance, the orientation period may be extended for a specified time. Upon successful completion of the orientation period team members will enter the "regular" employment classification. If team members are unable to demonstrate their ability to achieve a satisfactory level of performance during their orientation period their employment may be terminated.

## Team Member Classification

It is the intent of ConstructConnect to clarify the definitions of employment classifications so that team members understand their compensation and benefit eligibility. These classifications do not guarantee employment for any specified period of time. Accordingly, the right to terminate the relationship "At-Will" at any time, for any or no reason or cause with or without notice is retained and granted to both the team member and the Company.

Each team member is designated as either "exempt", "salary non-exempt" or "hourly" according to federal and state wage and hour laws. "Exempt team members" are not subject to the minimum wage and overtime requirements of state and federal law. "Salary non-exempt team members" and "hourly team members" are subject to the state and/or federal wage and hour laws and are paid at least the minimum wage and one and one-half times their hourly rate for all work in excess of forty (40) hours in a work week or as otherwise required by applicable state law.

In addition to the above categories, each team member will belong to one category: 

Temporary/Contractors Team Members
: are hired for a limited period of time and are ineligible to participate in the company's benefit program, training programs, and events.

Interns/Co-ops
: are enrolled in a school/academic institution with a minimum of a part-time status and are participating in the company's college program in an effort to gain experience in their specific field of interest.

Full-Time Regular Team Members
: are not hired for a specified limited period of time and who are regularly scheduled to work a minimum of thirty (30) hours (not including unpaid meal time) per week.  Generally, they are eligible for the company's benefit package and subject to the terms, conditions and limitations of each benefit program.

Part-Time Team Members
: work less than thirty (30) hours (not including unpaid meal time) per week. While they may receive all legally mandated benefits (such as social security and worker's compensation insurance) they are ineligible for all other Company benefit programs.

## Selection and Hiring of Candidates

Job Postings
: Most job opportunities will be posted internally to allow qualified internal applicants the opportunity to express interest and apply. However, promotions can be made within teams that do not require the position to be posted. At the discretion of the Talent Acquisition Team, open opportunities may be posted externally simultaneously. Opportunities will remain posted internally and externally until the position is filled or at the discretion of People & Culture (HR).

Internal Opportunities
: Team members may apply for or be promoted to career opportunities within their current team after six (6) months in their current role and after one (1) year outside their current team.  Any exception to the tenure requirement needs to be approved by the Chief People Officer. Team members must have a satisfactory performance record, and have not been issued any improvement plans in the previous six-month period to be eligible for internal career opportunities.

Team members must complete the "Internal Application" in its entirety and return it to People & Culture (HR). All applicants will be considered on the basis of their qualifications as they relate to the job description and performance in their current position. Only internal candidates who meet the qualifications and are in good performance standing will be interviewed. Candidates that are not selected for a transfer will be provided feedback regarding their interview with People & Culture (HR).

## Team Member Referral Program

Our team members are a critical element to our on-going recruiting
success through our Team Member Referral Program. We value candidate
referrals and reward our team members for helping the Company find the
right talent for our business needs.

### Referral Eligibility

- Active, regular, full-time team members are eligible to participate in the program.  
- Members of the management team and People & Culture (HR) are not eligible for referral rewards.  
- Referral of any applicant who has previously worked for the Company (including per diem) will not qualify for a reward.  
- Positions such as internships, temporary positions or summer positions are not eligible for rewards.  

### Referral Award

- The award amount will be $1,000 for team members who refer a qualified candidate, who is hired into a full-time position with the Company.  
- The candidate must indicate on their application that they were referred by a team member and the team member must complete and return the Team Member Referral form to the Talent Acquisition team.  
- The candidate must be actively employed with the Company for the award to be paid. The award will be paid once the candidate successfully completes 90 days of continuous employment.  
- For the award to be paid, the referring team member must be an active team member of the Company at the time of hire and 90 day anniversary of the candidate.  
- While every effort will be made for candidates to be interviewed for an appointment to a position, these decisions will be made based on the overall staffing needs of the Company as well as on the particular skills and experience of the candidate as determined by People & Culture (HR) and the Hiring Manager.

The People & Culture (HR) Team will make the final decision in determining eligibility for a referral reward.

## Employment of Relatives

ConstructConnect does not permit members of the same family to work at the Company. The Company has the right and will determine in all cases if a familial relationship exists. Relatives are defined as: parent, spouse, child, sibling, grandparent, grandchild, aunt, uncle, cousin, in-law or step relative, or any person with whom the team member has a close personal relationship such as a domestic partner, romantic partner or cohabitant. Team members who enter into a romantic relationship with another team member should disclose it to their manager and it will be reviewed accordingly. See the Relationships in the Workplace policy for additional information.

## Relocation

We offer relocation assistance on a case-by-case basis which can be tailored to meet the needs of both the team member and the business.  This assistance is designed to help new and transferred team members who are relocating their residences at the Company's request. Any team member who would like more information should contact the local People & Culture (HR) Team.

## Reinstatement After Break In Service

On occasion, team members are re-hired who have previously been employed by ConstructConnect. When a team member departs from the company, his/her manager will indicate the team member's rehire eligibility based on their rehire status indicated in the payroll system.

Team members re-hired less than one (1) year after separation will be restored to their original hire date. All applicants are required to complete the interview process as outlined in the interview and selection policy. With regards to eligibility for all other benefits, the plan documents for each benefit will govern whether previous service can be used to establish eligibility.

## Team Member Records

ConstructConnect is required to keep accurate, up-to-date employment records, to include electronic records, on all team members to ensure compliance with state and federal regulations and to keep benefits information current and to make certain that important mailings reach all team members. The Company considers the information in employment records to be confidential.  

The Company maintains employment records on each team member. The personnel file includes such information as the team member's job application, resume, records of training, documentation of performance appraisals and salary increases, and other employment records.  

Current team members will be permitted to review their personnel files by contacting People & Culture (HR). With reasonable notice the files may be reviewed in People & Culture (HR) in the presence of a member of People & Culture (HR). A team member may also make a request to have a copy of a document from their personnel file.

## Changes in Personal/Employment Status

Team members must inform the Company of any necessary updates to their personnel records. Information such as change of address, changed telephone numbers, emergency contact, marital status, number of dependents or names of covered beneficiaries and training/education should be changed in the ULTIPRO HR system.

## Payroll Policies

### Deductions

The Company adheres to a policy of strict compliance with the [Fair Labor Standards Act](https://www.dol.gov/agencies/whd/flsa). The Company will make deductions from the wages of its team members only as permitted by law. Among these are applicable federal, social security, Medicare, state and local income taxes, child support and garnishments. Making improper deductions from the wages of team members is strictly prohibited.  

Despite our best efforts to prevent improper deductions, it is possible that mistakes may be made.

If you have questions about deductions from your pay, if you believe you have been subject to any improper deductions, or if your pay does not accurately reflect your hours worked, please contact the Payroll Manager.

If you have not received a satisfactory response within five business days after reporting an incident, please immediately contact the Chief People Officer.

Every report will be fully investigated and if the Company determines that a deduction was taken improperly, the Company will reimburse the affected team member for the improper deduction.

The Company will not allow any form of retaliation against individuals who make good faith reports of alleged violations of this policy, or who cooperate in the Company's investigation of such reports, even if the reports do not reveal any errors or wrongdoing. Retaliation is unacceptable, and any form of retaliation in violation of this policy will result in disciplinary action, up to and including discharge. If the Company concludes that a team member has violated this Policy, corrective action will be taken, where appropriate, up to and including
discharge.

*Note to Exempt Team members*

As a general matter, exempt team members are not subject to partial-day
deductions from salary. However, exempt team members should be aware
that deductions from salary may occur under certain circumstances,
including but not limited to the following:

- Absences from work for one or more full days for personal reasons other than sickness or disability.  
- Offsets to amounts received by team members for jury duty, attendance as a witness, or temporary military leave; 
- Penalties imposed in good faith for infractions of safety rules of major significance; 
- Unpaid disciplinary suspensions of one or more full days imposed in good faith for infractions of workplace conduct rules; 
- Proportionate deductions from an exempt team member's initial or final week of employment in order to pay the team member only for the time actually worked during those weeks; and 
- Unpaid leave under the Family and Medical Leave Act.

### Payroll Frequency

Team members are paid by direct deposit semi-monthly. Scheduled hours worked through the pay date will be included on the payroll check. In the event that a regularly scheduled payday falls on a day off such as a holiday, team members will be paid on the work day prior to the regularly scheduled payday.

### Direct Deposit

For the sake of convenience and efficiency for both the team member and the Company, team members will have their payroll checks processed through electronic direct deposit or a payroll card in accordance with applicable law. *Not reporting bank account changes or closings could result in a delay in receiving a team member's direct deposit.*

## Career Discussions

We believe that every team member should have a clear and actionable career plan, as well as the opportunity to have ongoing conversations with their manager regarding their ongoing contributions to their team and the company.

Our process begins with the completion of the orientation period, at which time the manager and team member will meet to discuss the progress made to date, as well as to begin to outline career plan options.  

An in-depth career discussion will take place during the first quarter of each year. Short and long term career goals and action items will be created to begin to achieve your career goals. Following this, ongoing discussions with your manager will occur on a regular basis throughout the year to update your career plan and the progress you have made, as well as to note your contributions in your current role.  

## Salary Increases

Annual salary increases are not guaranteed. When provided, increases are based on business performance, individual performance and position and occur during the second quarter of the year.  

## Standards of Conduct

As an integral member of the Company, you are expected to accept certain responsibilities, adhere to acceptable business practices and exhibit a high degree of personal integrity at all times. This involves respecting the rights and feelings of others and refraining from any behavior that might be harmful to you, your coworkers or the Company. You are encouraged to observe the highest standards of professionalism at all times.

Since it is impossible to list in this Handbook every situation inconsistent with these principles, the absence of an illustration from this handbook will not prohibit the Company from taking action.

Issues related to sub-standard performance, attendance and conduct will be handled through the use of an Improvement Agreement, which is a tool designed for the manager and team member to identify the cause of the issue and create an action plan to improve/mitigate the issue. If a satisfactory improvement cannot be obtained and sustained, further action, up to and including immediate termination can occur when the Company believes, in its sole discretion, such action is warranted.  *Note: Some teams have team specific attendance policies and performance metrics that each team member is expected to adhere to.* 

The standard progression for an Improvement Plan is:

- Initial Performance Improvement Plan
- "Second Chance" Performance Improvement Plan
- Termination

*Note: Performance improvement processes may vary by team. Please ensure you are aware of your team's policy.*

The Company reserves the right to alter this progression on a case-by-case basis, taking all facts into consideration, including the severity and impact of the infraction.

# BENEFITS

Eligible team members at ConstructConnect are provided a wide range of benefits. A number of the programs such as social security, worker's compensation and unemployment insurance cover all team members in the manner prescribed by law. A team member's benefit eligibility is dependent upon a variety of factors, including team member classification. Details of these programs are found in this handbook.

A change in employment status that would result in loss of eligibility to participate in the health, dental or vision insurance plan may qualify a team member for benefits continuation under the Consolidated Omnibus Budget Reconciliation Act (COBRA).  

A Pre-Tax Benefit Plan (125 Cafeteria Plan) has been established under the Internal Revenue Code to allow the team member to pay for certain benefits *before* taxes are taken out of their paycheck. These benefits include: medical, dental and vision insurance.

During the annual open enrollment period, all benefit eligible team members may change insurance coverage options and level of coverage.  More detailed information about the insurance plans can be found in the summary plan description provided by the insurance carriers. Except under certain circumstances, once you have elected an insurance option and level of coverage, your election cannot be changed for the remainder of the year.

Team members are subject to all terms and conditions of the agreements between the Company and the insurance carriers. More detailed information about the Company benefits can be found in the summary plan description provided by the insurance carriers.  

## Paid Time Off (PTO)

**NOTE TO TEAM MEMBERS: Where applicable state laws or local ordinances (including Paid Sick Leave laws or ordinances) provide greater benefits than those described in this policy, the Company will comply with the state law/local ordinance.**

Paid Time Off (PTO) is an all-purpose time off policy for eligible team members to use for vacation, illness or injury and personal business.  Team members are accountable and responsible for managing their own PTO hours to allow for adequate reserves should the need arise. For your convenience, your PTO balance will show on the People & Culture (HR) Information System.

Annual PTO entitlement is based on your length of service. Each year, a regular full-time team member working 30 or more hours may take PTO depending upon his or her length of service, according to the PTO accrual table. Part-time, temporary, contract and co-op positions are not eligible for PTO. Length of service accrual increases will be done in the pay period of your service anniversary.

Our PTO year runs concurrently with the calendar year, from January 1 to December 31. We urge you to take all of your PTO time during each calendar year.

The amount of PTO days accrued per year increases with the length of employment as follows:

| Length of Service | PTO Days in the Calendar Year |
| --- | --- |
| 1 - 4 years | 17 PTO days/136 hours |
| 5 - 9 years | 22 PTO days/176 hours |
| 10 + years | 27 PTO days/216 hours |


Accruals
: Are based upon hours up to 2080 hours per year, excluding
overtime. The accruals will be awarded on the pay date for each pay
period. \*No PTO hours will accrue beyond the maximum accruals listed.

| # of PTO days per year | Avg \# of hours worked per day | Semi-monthly accrual in HOURS |
| --- | --- | --- |
|  17 | 8 | 5.67 hrs (max 136 per yr) |
|  22 | 8 | 7.33 hrs (max 176 per yr) |
|  27 | 8 | 9.00 hrs (max 216 per yr) |

PTO may be taken in no less than 1 hour increments.

Carry-over PTO
: Unused PTO days may be carried over year to year up to a maximum of 40 hours or as otherwise required by applicable state law. Carry-over PTO must be used by June 30 of the following year or will be forfeited, except where prohibited by applicable state law.

Borrowing PTO:
Until August 31<sup>st</sup> of each calendar year a team member may borrow up to 5 days of PTO. Beginning September 1<sup>st</sup> team members may only borrow what they will accrue for the remainder of the calendar year.

Unpaid time off will only be granted in conjunction with an approved leave. Absences not covered under an approved leave following the exhaustion of available PTO time will result in disciplinary action.

PTO time must be scheduled in advance and approved by your manager. Your manager can deny a PTO request at his or her discretion, including when business necessitates that you be available. Team members who are sick must notify their manager before the scheduled start of the workday.

New hires are not eligible to take PTO until the pay period following completion of 90 days of employment.

Temps or contract positions converting to full-time positions do not have to meet the 90 day waiting period to begin accruing and using PTO hours.

Rehired team members must meet the 90-day wait period if the break in service is more than 90 days.

PTO is paid at the team member's straight time rate. It does not include overtime or any special forms of compensation such as incentives, commissions and bonuses.

### PTO Entitlement for New Hires

In the first calendar year of employment, you will accrue PTO time based on hire date. The accrual is based on whether you are hired between the first and the fifteenth of the month or between the sixteenth and the end of the month, as follows:

| Month of Hire | Hired Between 1<sup>st</sup> and 15<sup>th</sup> | Hired Between 16<sup>th</sup> and end of Month |
| -- | -- | -- |
| January | 136 hours | 130.34 hours |
| February | 124.67 hours | 119.01 hours |
| March | 113.33 hours | 107.67 hours |
| April | 102 hours | 96.34 hours |
| May | 90.67 hours | 85.01 hours |
| June | 79.33 hours | 73.67 hours |
| July | 68 hours | 62.34 hours |
| August | 56.67 hours | 51.01 hours |
| September | 45.33 hours | 39.67 hours |
| October | 34 hours | 28.34 hours |
| November | 22.67 hours | 17.01 hours |
| December | 11.33 hours | 5.67 hours |

### PTO Entitlement During a Leave

During a leave of absence you do not accrue PTO time. Accrued PTO time will be applied to any leaves and will run con-currently. After the exhaustion of such PTO, the remaining leave will be unpaid.

### PTO Termination of Employment

Upon voluntary termination of employment, a team member must give a 2 week notice in order to receive payment for any accrued, unused PTO time through the last day of employment where permitted by applicable law. No PTO may be used during the team member's resignation period. PTO payouts will be paid on the next regularly scheduled pay period.

If a team member is involuntarily terminated because of non-compliance with any local, state or federal law or violation of the Company's policies, PTO time will not be paid out where permitted by applicable law.

Any PTO time taken that was not earned is treated as a salary advance owed to the Company by you and will be deducted from your last paycheck, as permitted under applicable law.  

## PTO Donation 

The Company recognizes team members may have a family emergency or a personal crisis that causes a severe impact to them resulting in a need for additional time off in excess of their available PTO time. To address this need all eligible team members will be allowed to donate PTO time from their unused accrued balance to co-workers in need in accordance with the policy. The donation made will go to a general pool and cannot be specified for a particular team member. Federal and State regulations will govern this process in terms of amount and type that can be donated. This policy is strictly voluntary.

Contact People & Culture (HR) for more details on the policy and donation approval process.

## Holidays 

ConstructConnect recognizes 10 paid holidays throughout the year:

| CORE HOLIDAYS | DAY RECOGNIZED |
| -- | -- |
|  New Year's Day | January 1<sup>st</sup> |
|  Memorial Day | Last Monday in May |
|  Independence Day | July 4<sup>th</sup> |
|  Labor Day | First Monday in September |
|  Thanksgiving | Fourth Thursday in November |
|  Day After Thanksgiving | Fourth Friday in November |
|  Christmas Eve | December 24<sup>th</sup> |
|  Christmas Day | December 25<sup>th</sup> |
|  Floating Holidays | 2 days per year |

Team Members hired between October 1<sup>st</sup> and November 15<sup>th</sup> will receive one (1) Floating Holiday to use in the first calendar year. Team Members hired between November 16<sup>th</sup> and December 31<sup>st</sup> will not receive Floating Holidays their first calendar year.

For team members who are scheduled to work at least 30 but fewer than 40 hours per week, holiday pay will be calculated by multiplying the team member's base hourly rate by the number of hours per day the team member is normally scheduled to work.  

Dependent upon the calendar, some holidays are observed on their actual day, and others may be assigned to a different date to take advantage of long weekends. A recognized holiday that falls on a Saturday will be observed on the preceding Friday. A recognized holiday that falls on a Sunday will be observed on the following Monday.

If a recognized holiday falls during an eligible team member's paid absence such as vacation, holiday pay will be provided instead of the PTO benefit that would otherwise have applied.  

The day before a holiday is considered a normal work day with standard work hours. PTO must be utilized for any hours not worked.

If a recognized holiday falls during an eligible team member's leave or suspension such as FMLA, military, or personal leave, holiday pay will *not* be provided.

Salary non-exempt and hourly team members are required to work a complete day on the last scheduled day before the holiday, and on the first scheduled day of work after the holiday to be paid for that holiday. Scheduled PTO or excused absences are exceptions to this requirement.

Paid-time off for holidays will not be counted as hours worked for the purposes of determining whether overtime pay is owed.  

Floating holidays can only be used in full one (1) day increments and are available beginning on your first day of employment. Except where prohibited by applicable state law, unused floating holidays will be forfeited December 31<sup>st</sup> of each year.

## Community Outreach Volunteer Benefit

ConstructConnect believes in giving back to the community and has created a community outreach committee to achieve this goal.

Team members are encouraged to support our community during working hours and may take up to two hours per month to do so. New hires are not eligible to use volunteer hours until the completion of their orientation period. Unused hours will accumulate if not used the prior month but will not carry over into a new calendar year. Note: usage of volunteer hours before they are accrued will be considered on a case by case basis and should be pre-approved through People & Culture.

Team members may sign up for the volunteer hours through the Company's Community Outreach Program or present information pertaining to the organization for which they wish to volunteer to their manager for approval. Managers should be made aware that the team member will be volunteering at least 48 hours in advance. Your manager can deny a request at his or her discretion, including when business necessitates that you be available. All hours taken for this purpose should be recorded in ULTIPRO as "Volunteer Hours". (hours used are calculated based on departure and return time to the office)

## EAP Work/Life Assistance Program

Today's world is challenging as we strive to balance the demands of work, family and our personal needs. We recognize the impact of daily pressures on an individual's performance-both personally and professionally. Our Team Member Assistance Program (EAP) is a confidential program that allows team members to access EAP free of charge.

Contact People & Culture (HR) for additional information on our EAP program.

## Local Activity Discounts

Local activity discounts offer team members the opportunity to participate in a broad range of leisure activities. Team members can enjoy discounts and reduced admissions at major amusement centers such as major Theme Parks. Other discounts may include admissions to selected movie theaters, zoos and museums.

These programs vary from location to location. Visit the ConstructConnect Intranet for a list of discounted activities available at your location.

## Worker's Compensation 

ConstructConnect provides a worker's compensation program for their team members. This program covers most injuries or illnesses sustained in the course and zone of employment that require medical, surgical or hospital treatment. Subject to applicable legal requirements, worker's compensation insurance provides benefits after a short waiting period or, if the team member is hospitalized, immediately.

Injuries that occur on the job should be immediately reported to the team member's manager and a member of People & Culture (HR). Off-site team members should report an accident to a member of People & Culture (HR) immediately. If an accident occurs, team members who witness the accident should contact the appropriate medical personnel and should not attempt to transport or make medical decisions on their own. An Accident Report then must be completed within 24 hours of the accident.

A team member who requires medical attention at a medical facility because of a worker's compensation injury may be required to take an alcohol/drug screen test at the facility. A team member that tests positive or refuses to submit to chemical testing may be disqualified for compensation and benefits under the Worker's Compensation Act.

## Time Off to Vote 

ConstructConnect encourages its team members to participate in the election of government leaders. Therefore, if a team member does not have sufficient time outside their regular work hours to vote, a team member may schedule PTO to take time off at the beginning or end of the workday to exercise this right as per the Company's PTO policy. Where state law provides for time off to vote, ConstructConnect will comply with such state law.

## HIPAA Title II Privacy Regulations 

In compliance with federal regulations implementing the Health Insurance Portability and Accountability Act (HIPAA), the Company will take steps to ensure the privacy of Protected Health Information (PHI) of our team members. Security and privacy safeguards that are set up will protect the confidentiality, integrity and availability of PHI. If you would like more information contact People & Culture (HR).  

# COMPENSATION

## Timekeeping

All salaried non-exempt and hourly team members must accurately and consistently record their time worked through the use of the Company's electronic timekeeping system. Exempt team members are not required to record hours worked and are expected to work the number of hours required to complete their assigned tasks.

The electronic timekeeping system is used to compute earnings and is kept as a permanent record. Each non-exempt team member is responsible for accurately reporting the time they began and ended their work, as well as the beginning and ending time of each meal period. They should also record the beginning and ending time of any split shift or departure from work for personal reasons.

It is mandatory that all clock-in and clock-out activity of a team member's time be performed at an authorized computer ONLY. Logging in from any other access point may result in the termination of employment.  

Altering, falsifying, tampering with time records, working off the clock, or recording time on another team member's time record are one of the most serious violations of the Company's rules and as a result may lead to immediate termination.

When a team member clocks in they are working on the job performing assigned duties. This may be different from the time they arrived or left the workplace. *All overtime must be scheduled and approved in advance by their manager. (See Overtime below.)*

## Overtime

When business requirements or other needs cannot be met during regular working hours, team members may be scheduled to work overtime hours.  When possible, advance notification of these mandatory assignments will be provided. Overtime assignments will be distributed as equitably as practical to all team members qualified to perform the required work.  

*All overtime must be scheduled and approved via e-mail by the team member's manager in advance of working the overtime hours when possible.  When not possible, notification of overtime worked must be provided to the team member's manager via e-mail within 24 hours.* 

All non-exempt team members will be paid one and one half times their regular rate for all hours worked in excess of 40 in one work week or as otherwise required by applicable state law. Hours for which a team member is paid for but which they do not actually work (such as holidays, PTO, jury duty, etc.) are not counted as hours worked for computing overtime payments. Exempt team members are not eligible to be paid overtime.

## Non-Exempt Travel

Some team members in non-exempt positions may be required to travel in the course of their responsibilities. Team members in positions classified as non-exempt under the Fair Labor Standards Act are eligible for compensation for the time they spend traveling. The compensation a team member receives depends upon the kind of travel and whether the travel time takes place within normal work hours or outside of normal work hours.

"Normal work hours", for the purposes of this policy, are based on your regular hours of work. Travel from home to work at the beginning and end of the regular workday day is not considered work time.

Home to Work on a Special One Day Assignment in another City or Location
: In this situation, a team member who regularly works at a fixed location in one city is given a special one day assignment in another city and returns home the same day. The time spent in traveling to and returning from the other city is considered work time. Deducted from the total work time will be time that the team member would normally spend commuting to their regular work site.

Travel that is all in a Day's Work
:Time spent by a team member in travel as part of their principal activity, such as travel from work site to a customer location during the workday, is work time and must be counted as hours worked.

Travel Away from the Home Community
: Travel that keeps a team member away from home overnight is travel away from home. Travel away from home is clearly work time when it cuts across the team member's workday. The time is not only hours worked on regular working days outside of regular working hours but also during corresponding hours on nonworking days.  Time spent in travel away from home outside of regular working hours as a passenger on an airplane, train, boat, bus, or automobile is not considered working time and therefore is not compensable.

For more information regarding this policy please contact the People & Culture (HR) Team.

## Bonuses/Commissions 

Team members will be paid commissions and/or bonuses according to either an individual, position or Team commission and/or bonus plan.  Team members who have signed a commission and/or bonus plan must be actively employed with the Company at the time customer payments are received by the Company. Commission payments will be paid according to the commission and/or payment schedule reflected in the bonus/commission plan document. Bonus payments for team members not actively employed are reviewed and paid based on leave of absence / exit timing. 

## Pay Advances

ConstructConnect discourages any advancement of pay not yet earned and any exception will require an extraordinary or emergency situation.  Extraordinary or emergency situations include foreclosure of primary housing, utility services disconnection notice, repossession of primary vehicle and medical expenses.

A team member in need of a pay advance must submit a request to their manager, using the Company's Standard Pay Advance Request Form. Requests must have the approval of the Chief People Officer (HR), the EVP, Finance and President. The advance will not exceed one week's net pay with repayment terms not to exceed two months. Team members will be limited to one pay advance per year.

# WORK ENVIRONMENT

## Hours of Work

Work schedules for team members vary throughout our Company. Managers will advise team members of their individual work schedules. Staffing needs and operational demands may necessitate variations in starting and ending times, as well as variations in the total hours that may be scheduled each day and week.

The standard work week is forty (40) hours. The standard workday is eight (8) hours for non-exempt workers. Workday lengths for exempt team members are determined primarily by the hours required to accomplish their current workloads. General business hours are from 8:00 a.m. to 5:00 p.m. daily.

Unless otherwise provided by applicable law, an unpaid meal period is provided to any team member who works a minimum of six (6) hours per day (minors five (5) hours). Non-exempt team members are required to clock out for their lunch periods.  

Unless otherwise provided by applicable law, non-exempt team members receive two fifteen-minute paid break periods for each full workday. If deemed necessary the Company can modify a team member's starting and quitting time as well as the number of hours worked to accommodate business needs.

## Attendance 

Team members are expected to arrive at work on time each scheduled work day. Punctuality and regular attendance are basic and essential requirements for satisfactory performance.

We recognize the need for team members to be absent from work due to illness or the need to take care of personal business during the normal workday. The Company instituted Personal Time Off (PTO) to provide for these needs as they arise. Having provided for these situations, it is important to remember that excessive absenteeism or breaks, arriving late, and/or leaving early causes disruption to the business and our customers.

You are expected to have a consistent start and end time to your work schedule that has been established by your manager. Team members who are going to be absent, late or leaving early from work are responsible for notifying their managers as soon as possible. Notification should be done prior to the start of the shift and at least 24 hours' notice should be given for planned PTO. Failure to notify your manager is seen as a violation of the policy.

Team members that are absent for three (3) or more consecutive days due to illness, injury or disability, may be required to provide written documentation from an attending physician.  

Team members that have been absent for three (3) consecutive days without direct notification to their manager or People & Culture (HR) will be considered to have voluntarily terminated their employment. If absences or attendance related issues become problematic or disruptive to the business, they will be addressed.

Issues related to attendance may be handled through the use of an Improvement Agreement, which is a tool designed for the manager and team member to identify the cause of the issue and create an action plan to improve/mitigate the issue. If a satisfactory improvement cannot be obtained and sustained, further action, up to and including immediate termination can occur when the Company believes, in its sole discretion, such action is warranted. ***Note: Some Teams have Team specific attendance policies and performance metrics to which each team member is
expected to adhere.***

## Flexible Work Schedule

The company is committed to a work environment that accommodates our People First/Family First values. Each team offers a flexible/telecommuting schedule based on business sustainability and individual performance.

The following basic requirements must be met for all teams:

- Team members must have satisfactory attendance and cannot be on a Performance Improvement Plan.  
- The workweek for all full-time regular team members is 40 hours per week and 8 hours a day.  
- There will be times when team members are required to depart from the flexible work schedule to accommodate changing situations and staffing needs. Reasonable notice will be provided when possible.  
- The day before a holiday falling on a work day is treated as a normal 8 hour business day.

## Friday Early Release

In keeping with our People First/Family First philosophy, we offer our team members the ability to leave 1.5 hours early on Friday.

- Team members may leave 1.5 hours earlier than their normal schedule as long as individual performance goals are being met and 6.5 hours are worked for the day.  
- Team members on a Performance Improvement Plan are excluded from this benefit.  
- Managers reserve the right to ask team members to stay for their entire schedule due to business needs.  
- Early release and PTO cannot be combined. If PTO is taken on a Friday the entire day must be coded as PTO (8 hours). If a partial day is taken, PTO and hours worked must equal 8 hours. Lunch can be taken, but 6.5 hours should be worked that day and if salaried non-exempt, recorded as such in Ultipro.

## Personal Appearance 

It is the intent of ConstructConnect that all team members dress for their own comfort during work hours provided that the professional image of our Company is maintained. Team members may dress in casual clothing that is clean, neat, free of rips and tears and is in good taste.

Provocative or potentially offensive dress or clothing that is ill-fitting, or has inappropriate slogans, sayings or pictures is not considered to be in good taste. Inappropriate tattoos are included in this category.

Team members who are meeting with our customers must dress in appropriate business attire.

The Company reserves the right to determine whether or not dress is inappropriate and may send team members home to change under these circumstances. Team members will not be paid for this time.

## Drug Free Workplace 

The use of illegal drugs and alcohol, and the abuse of legal prescription pharmaceuticals, account for tremendous losses in efficiency, attendance and costs of Company-provided health care. These abuses also diminish the safety of all team members and visitors, impair the reputation of the Company, and violate state and federal laws. In addition, the use and abuse of drugs and alcohol can have severe health consequences and lead to the destruction of family unity. For these reasons, the Company has adopted a no tolerance drug and alcohol policy.  With this policy, it is the intention of the Company to use every lawful means to establish and maintain a drug and alcohol free workplace.

Illegal drugs are substances that are controlled or outlawed, are not obtainable by lawful methods, or are legally obtainable but were not obtained in a lawful manner.

This policy prohibits the use, sale, manufacture, distribution or possession of alcohol or illegal drugs, drug paraphernalia or any combination thereof, on any Company premises or at any location where the team member is performing their job duties and includes Company vehicles on or off Company premises. Included within this prohibition is medical or recreational marijuana, which remains illegal under federal law. Under appropriate circumstances and in the absolute discretion of the Company, any illegal substance may be turned over to the appropriate law enforcement agency and may result in criminal prosecution. Violation of this policy will subject the team member to disciplinary action up to and including immediate termination, and may have legal consequences.

To the extent permitted by applicable state law, the Company shall have the right to require any team member to submit to drug and/or alcohol testing under the following circumstances:

1.  *Post-Accident* - Where the team member was involved in an accident that resulted in property damage or physical injury, requiring professional medical treatment beyond first aid.
1.  *Reasonable Suspicion* - Upon the belief of management that the team member may have alcohol or illegal drugs in their system while at work or while performing their job duties away from the workplace.

This list is not meant to limit the circumstances under which a drug or alcohol test may be required. The Company can test for the presence of alcohol or illegal drugs for other lawful purposes.

For purposes of this policy, a positive (or failure) of a drug or alcohol test is defined as having the presence of any detectable amount of an illegal drug or alcohol in the team member's system when tested.  Similarly, refusal to submit to a drug or alcohol test when requested by the Company, or any attempt to interfere with the test or alter the sample, also constitutes failure of the test and will result in a team member subject to discipline up to and including termination.

Independent Contractors and temporary team members for the Company will be required to adhere to this policy.

If a team member is using medication prescribed by a licensed physician, he is responsible for obtaining assurances from that physician that the medication will not impair their judgment or ability to safely and efficiently perform their job duties. If impairment is possible the team member should report the use to their manager prior to reporting to work.

Any team member who knows or believes that there is unlawful involvement by other team members, vendors or guests with illegal drugs or alcohol contrary to this policy should immediately refer this information to their manager or to People & Culture (HR). The Company will utilize all lawful investigative techniques in response to this information.  Evidence obtained by the Company of the unlawful use, manufacture, trafficking, distribution or possession of controlled substances will be provided to the appropriate law enforcement authorities.

Team members who are convicted of off-the-job drug or alcohol related crimes may be considered to be in violation of this policy. In deciding what action to take, management will take into consideration the type of criminal conviction involved and all other factors relating to the circumstances of the criminal conviction and the impact on the company.

The Company recognizes that drug or alcohol abuse may, in some cases, be an illness or mental health problem. Team members who need help in dealing with these problems are encouraged to seek assistance voluntarily before the situation requires management intervention. Team members who come forward in this manner may be allowed to take unpaid leave to participate in a rehabilitation program. After satisfactorily completing the program and furnishing proof to that effect, the team member will be allowed to agree to and sign a conditional last chance agreement. Further involvement in the use of illegal drugs or alcohol in violation of this policy will result in immediate termination

Information related to substance test results and/or requests for team member assistance related to a substance problem is confidential. Access will be limited to designated Company personnel.  

This policy is not meant to prohibit the consumption of alcohol when and where it is specifically authorized by management as part of a Company function.

This policy does not limit the right of the Company to invoke disciplinary action for any unauthorized activity not enumerated above.  

## Business Conduct

As a team member of the Company, you have an obligation to conduct business according to the ConstructConnect Code of Business Conduct described below:

- You will not use the Company's name in connection with any outside venture without the prior written approval of the CEO.  
- You will not misrepresent the Company in any way in an effort to obtain information from or about our competition.  
- Company team members may not enter into contracts on behalf of the Company unless their position is granted the authority to do so by Corporate resolution.  
- You will not make any illegal payments, directly or indirectly, in order to obtain or retain business for the Company.  
- All accounting entries, invoices and other documentation shall correctly describe the transactions to which they relate. There will be no omissions from the books and records, nor any hidden accounts or funds.  
- You must not disclose or misuse Confidential Information, as defined elsewhere in this Handbook.  
- You may not use the Company's name, property, services or resources for your personal benefit without prior approval from the CEO.  
- You will not falsify employment or other company records.

## Ethics and Conflicts of Interest 

Team members are expected to use good judgment, adhere to high ethical standards and avoid situations that create an actual or perceived conflict between their personal interests and those of the Company.  ConstructConnect. requires that the transactions team members participate in are ethical and within the law, both in letter and in spirit.

The Company recognizes that different companies have different codes of ethics. However, just because a certain action may be acceptable by others outside of ConstructConnect as "standard practice", that is by no means sufficient reason to assume that such practice is acceptable at our Company. There is no way to develop a comprehensive, detailed set of rules to cover every business situation. The tenets of this policy outline some basic guidelines for ethical behavior at the Company.  Whenever team members are in doubt, they should consult their manager or People & Culture (HR).

Conflicts of interests or unethical behavior may take many forms including, but not limited to, the acceptance of gifts from competitors, vendors, potential vendors or customers of the Company. Gifts may only be accepted if they have a nominal value and only on appropriate occasions (for example, a holiday gift). Team members are cautioned not to accept *any* form of remuneration or non-business related entertainment, nor may team members sell to third parties any information, products or materials acquired from the Company. Team members may engage in outside business activities, provided such activities do not adversely affect the Company or the team member's job performance and the team member does not work for a competitor, vendor or customer. Team members are prohibited from engaging in financial participation, outside employment or any other business undertaking that is competitive with, or prejudicial to, the best interests of Company.  Team members may not use proprietary and/or Confidential Information, as defined elsewhere in this Handbook, for personal gain or to the Company's detriment, nor may they use assets or labor for personal use.

If a team member or someone with whom the team member has a close personal or familial relationship has a financial or employment relationship with a competitor, vendor, potential vendor or customer of the Company, the team member must disclose this fact in writing to their manager and the Chief People Officer. The Company will determine what course of action must be taken to resolve any conflict it believes may exist. If the conflict is severe enough, the Company may be forced to ask the team member to tender their resignation. The Company has sole discretion to determine whether such a conflict of interest exists.

Should any team member become aware of what they believe to be unethical or illegal action with any connection to the Company or its business associates, that team member has the duty and responsibility to immediately report that information to their manager and/or People & Culture (HR) for investigation. Failure to do so will subject the team member to discipline up to and including termination. All team members should realize that any wrongdoing is counter to our vision, mission and values and will therefore, not be tolerated. Team members are encouraged to seek assistance from their managers with any legal or ethical concerns. However, the Company realizes this may not always be possible.  As a result, team members may contact People & Culture (HR) to report anything that they cannot discuss with their manager.

## Alcohol at Company Functions

The Company is committed to limiting the consumption of alcohol by team members at company functions. Excessive alcohol consumption can endanger the health and safety of team members and result in inappropriate behavior that harms both the reputation of the individuals involved, as well as the Company.

Company functions to which this policy applies include but are not limited to company parties, outings, and team events. Any off-site functions will be held in appropriately licensed facilities, with drinks served by professional bartenders. Team members who choose to drink alcoholic beverages at company functions will be limited to two alcoholic beverages purchased by the company. All other beverages consumed will be at the cost of the team member. Alcoholic beverages may not be served in offices or work areas. Alcoholic beverages may not be consumed during office hours or at lunch or breaks.

Team members who choose to drink alcoholic beverages are expected to behave in a professional manner with usual business standards and all company policies and codes of conduct. Failure to do so may result in disciplinary action up to and including termination.

## Solicitation & Distribution

Solicitation by a ConstructConnect team member of another team member is prohibited during the working time of either person. Working time is defined as time when team member's duties require that they be engaged in work tasks. Distribution of printed material or literature of any nature shall be limited to non-work areas at non-work times. No literature shall be posted anywhere on the premises without the authorization of People & Culture (HR). Solicitation and/or distribution of material on Company property by persons not employed by the Company are prohibited at all times.

## Team Member Work Space

Good work habits and a neat place to work are essential for job safety and efficiency. Team members are expected to keep their work space organized and materials in good order at all times. Offensive materials should not be displayed in a team member's work space at any time. An example of such prohibited offensive material is abusive or profane language, racial, ethnic, sexual, religious, disability or age related slurs, or other forms of verbal assault, horseplay, bullying, offensive teasing, threats, coercion, demeaning gossip, and unwanted proselytizing.

Team members are expected to use proper care when using the Company's property and equipment. No property may be removed from the premises without proper authorization of management. Unsafe, broken, lost or damaged property or equipment should be reported to the team member's manager immediately.

Personal belongings brought onto Company premises are the team member's responsibility. While the Company will do all it can to protect a team member's property, it cannot be held responsible for the loss or theft of personal belongings. If a team member finds property missing or damaged, they should immediately report it to their manager and the manager will report it to People & Culture (HR). The Company, as stated elsewhere in this Handbook, can and will perform routine inspections or searches as it deems necessary without prior notice or prior consent by a team member. Because even a routine inspection or search might result in the viewing of a team member's personal possessions, team members are encouraged not to bring any item of personal property to the workplace that they do not want revealed to others in the Company.

## Tobacco-Free 

The use of tobacco products including, but not limited to cigarettes, smokeless cigarettes, cigars, tobacco pipes and all forms of smokeless tobacco, is prohibited inside any of the Company's facilities or vehicles except for outside designated areas ONLY. For purposes of this policy, the prohibition of tobacco use is extended to include electronic cigarettes and/or any item, device or method of administering nicotine or any other drug by inhalation, whether a drug is included in the formulation or not.

This policy relates to all work areas at all times, including before and after normal working hours. Team members may use tobacco products at scheduled break periods in the designated area ONLY. All cigarette butts must be placed in the ashtray located in the designated smoking area.

## Use of Telephones & Recording Devices 

Office telephones are a vital part of our business operation. Because of the large volume of business transacted by telephone, personal use of the telephone or cellular phones should be limited and personal calls should be brief.

While at work team members are expected to exercise the same discretion in using personal cellular phones and personal digital assistants as is expected for the use of Company phones. Excessive personal calls during the work day, regardless of the phone used, can interfere with team member productivity and can be distracting to others and is therefore strictly prohibited as a violation of the Company's rules and regulations. Team members are expected to make personal calls on non-work time when possible and to ensure that friends and family members are aware of the Company's policy. *This policy includes the usage of text messaging*. Team members that have excessive phone usage, including text messaging, for personal calls on Company phones or personal cell phones will be subject to disciplinary action, up to and including termination. In addition, use of any company or personal phones in the restrooms is strictly prohibited.

Team members are not prohibited from photographing, taping or recording, or attempting to record any person, document, conversation, communication, or activity that occurs at, or is related to the business activities of the Company. However, the following are examples of restrictions that do apply:

- Working time is for work. Unless photographing, taping or recording is required as part of a job function you are performing, it should not be done during your working time. Photographing, taping or recording during non-working time is permitted, subject to the other qualifications below.  
- If photographing, taping or recording would violate other Company policies, such as the Company's policy prohibiting discrimination based on any protected characteristics, it is not permitted at any time.  
- Photographing, taping or recording Company trade secrets or other confidential information, such as customer lists, internal financial information and account numbers, proprietary technical formulas and processes, and protected medical records, is not allowed at any time.  
- Photographing, taping or recording that infringes on reasonable expectations of privacy, such as the privacy of co-workers using restrooms and locker rooms, is not permitted.  
- Recording coworkers without their knowledge or consent may infringe on their privacy rights. Nonconsensual recordings may be viewed as particularly intrusive and unreasonable, and result in claims of illicit harassment; be aware that some religions may prohibit the taking of photographs or videos of members of that religion.  
- Some jurisdictions have laws that prohibit recordings of any person without their knowledge or consent. Recording in any manner that violates state or federal law is prohibited.

Photographing and videotaping is not restricted when team members are acting in concert for their mutual aid and protection and no overriding contrary interest is present. Consistent with the foregoing, recordings may include team members documenting workplace conditions, or other terms or conditions of employment.

## Travel Policy 

All travel arrangements should be made through Concur at [www.concursolutions.com](http://www.concursolutions.com) or by contacting Teplis Travel. It is the traveler's responsibility to report his or her actual travel expenses in a responsible ethical manner, in accordance with the regulations set forth in the complete Travel policy.  Submission of travel expenses should be completed within 60 days of the trip and approved by the team member's immediate supervisor. Please refer to the complete Travel policy for complete details of the Company's travel and entertainment policies. (See The Loop for more details)

## Driving 

Team Members required to drive for company business must at all times meet the following criteria:

- Team members must have a current, valid driver's license for the state in which the team member lives.  
- Team members who have occasion to drive on company business must report the loss of driving privileges to their immediate Manager and People & Culture (HR) within 24 hours of when it occurred.  
- Team members must maintain a clean driving record, i.e., must remain insurable under the company's liability insurance policy.  
- Team member must ensure that they have the state minimum insurance on their own vehicle at all times.  
- Team member must provide a copy of their driver's license and proof of insurance upon request.

### Alcohol and Drug Abuse

Driving a vehicle while under the influence of alcohol or any drugs or narcotics is strictly prohibited and subject to disciplinary action including termination of employment.

### Cellular Use While Driving

Team members whose job responsibilities include regular or occasional driving and use their personal cellular telephone for business-related work are expected to put safety first. Operators driving vehicles that are not equipped with a "hands-free" application must pull over safely, park, before engaging in any cellular phone conversations. Use of other electronic devices (laptops, iPad, iPod, etc.) is prohibited while driving. Texting or reviewing texts while driving is likewise strictly prohibited. This policy is in effect for your safety and the safety of others, as well as the safety of the Company's property. Where applicable state law prohibits the use of cell phones of any kind while driving, Company policy will adhere to the state law.

Team members who are charged with traffic violations, or cause accidents or injuries resulting from their use of personal or Company-issued cellular telephones or other devices while driving, will be solely responsible for all liabilities, fines, etc. that result, to the extent permissible under the law.

Team members whose job responsibilities do not specifically include driving as an essential function, but who are issued a Company-provided cellular telephone for business use or who use their personal cellular telephone for business use, are also expected to abide by the provisions of this policy.

## Relationships in the Workplace 

ConstructConnect strongly believes that an environment where team members maintain clear boundaries between team member personal and business interactions is most effective for conducting business. Since dating relationships that include a manager and/or supervisor with another team member may lead to apparent favoritism or actual conflicts of interest, they are strictly prohibited in the same Team at any level (higher or lower) or in the same line of authority that may affect employment decisions.

Also, having two non-management or management team members trying to keep the proper balance at work between objective business professionalism and subjective romantic interest is clearly not in the best interest of the company or its team members so any romantic link between two non-management team members is discouraged and may lead to job action if causing any problems.

If any team member relationship, either personal or work-based, becomes disruptive or, in the discretionary view of management, interferes with the proper and efficient conduct of business, the team members involved will be counseled one time about the issue and given the opportunity to jointly correct the problem. If this correction does not occur, the team members involved will be given the opportunity to decide which team member will voluntarily leave employment. If this is not accomplished Company management will make a decision whether one or both team members will leave employment.

In keeping with policies on Unlawful Harassment and Workplace Violence described elsewhere in this Handbook, team members are strictly prohibited from engaging in physical contact that would in any way be deemed inappropriate by a reasonable person while anywhere on Company premises, whether during working hours or not.

## Call Monitoring 

Team members need to be customer service oriented by treating customers in a courteous and respectful manner at all times.  To measure and evaluate customer service, ConstructConnect may observe, listen and record team members' telephone conversations made in the normal course of business.  The monitoring of telephone conversations will be random and periodic and will be used for training purposes. Advanced notice will not be given prior to monitoring telephone conversations.  Any team member whose telephone conversation may be observed, listened to and recorded will be asked to sign a consent form authorizing the monitoring.  This form will be completed during the team member's orientation or at the time the team member is transferred to a position where the Company wants consent for monitoring.

Monitoring will be limited to business-related objectives.  Monitoring will be discontinued if it becomes apparent that the team member is engaged in discussing personal matters

## Outside Employment 

While ConstructConnect does not prohibit team members from having a second job, secondary employment must be reported if it presents a conflict of interest and/or will have an adverse impact on the Company.  Conflict of interest also includes working directly for a competitor within the same industry or job function.

## Media Communications 

To ensure that the Company communicates with the media in a consistent, timely and professional manner about matters related to the Company, you should notify your manager and/or Chief Marketing Officer that you have been contacted by the media whenever you are asked to speak on behalf of the Company so that the Company knows that a media inquiry has been made. Do not respond to media inquiries on the Company's behalf without authorization. This rule does not prevent you from speaking with the media, but you should not attempt to speak on behalf of the Company unless you have specifically been authorized to do so by an officer of
the Company.

## Verification of Employment 

People & Culture (HR) will respond in writing only to reference check inquiries. Responses to such inquiries will confirm only dates of employment and position(s) held. No employment data will be released without a written authorization and release signed by the individual subject of the inquiry. People & Culture (HR) is designated to respond to reference check inquiries. All calls, contacts and written inquiries concerning current or former team members should be referred to People & Culture (HR).

# LEAVES OF ABSENCE

## Bereavement Leave

Death of a family member or friend is often an unexpected event that we all must deal with at some point in our lives.  Therefore, ConstructConnect recognizes the need for time away from work for funeral preparation and funeral attendance.  A team member who wishes to take time off due to a death of a family member or friend should notify their manager immediately. 

Paid bereavement leave is granted up to five (5) days for the death of a close relative. For purposes of this policy, a close relative is defined as spouse, domestic partner, child, step-child, father, mother, step-parent, parents-in-law, brother, sister, grandchild, daughter/son-in-law, grandparent and brother/sister-in-law.

In the event of the death of other family members or friends, the team member may request to take Paid Time-off to pay their respects.  

The amount of Bereavement Leave requested by a regular full-time team member that may be granted is determined by the team member's involvement in the funeral preparation and one day Bereavement Leave granted must be used to attend the funeral.  Bereavement pay granted is calculated based on the team members base pay rate at the time of absence and is paid for their normal work schedule eight (8) hours and will not include overtime.  

Bereavement leave will normally be granted unless there are unusual business needs or staffing requirements. A team member may, with their manager's approval, use any available Paid Time-Off and/or Personal Leave for additional time off as necessary.  Bereavement pay will not be granted for team members on FMLA leave, personal leave, military leave, and maternity leave, suspension or if it falls on a paid holiday.  

The Company may request at any time, pertinent information including the deceased relative's name, the name and address of the funeral home, and the date of the funeral.

## Jury Duty and Witness Service Leave 

The Company supports team members called to fulfill their civic duty to serve on jury duty. Full-time and part-time team Members are eligible to request up to 2 weeks of paid jury duty leave over any 1-year period.

Should extraordinary circumstances exist, at the time of your call to jury duty, which would make your absence severely detrimental to the operation of our Company, we can and will contact the court to request that your service be postponed.

Team members may keep any compensation they are paid for jury duty or witness service. They will be paid their straight time base rate of pay for all hours missed up to eighty (80) hours due to jury duty or witness service on behalf of the Company in addition to any compensation received from the court. If team members are required to serve jury duty beyond the period of paid jury duty leave, they may use any available PTO or may request an unpaid jury duty leave of absence.

Team members are required to give a copy of the subpoena, or notice or summons from the court, to People & Culture (HR) prior to serving. If the team member is released from jury duty with 4 or more hours left in their shift they must report to work for the remainder of their shift.  Documentation of daily hours served from the court must also be given to People & Culture (HR) upon a team member's return.

Team members called to testify as a voluntary witness at the request of the Company, by subpoena or otherwise, will be paid for the day or days in which the court requires attendance. If team members are subpoenaed to appear in court as witnesses, but not at the request of the Company, they will be excused from work in order to comply with the subpoena but will not receive witness leave pay and may take available PTO time.

Where applicable state law provides additional benefits beyond those described in this policy, the state law will apply.

## Family and Medical Leaves of Absence (FMLA)

The Company will grant family and medical leave in accordance with the requirements of applicable state and federal law in effect at the time the leave is granted. Although the federal and state laws sometimes have different names, the Company refers to these types of leaves collectively as "FMLA Leave." In any case, team members will be eligible for the most generous benefits available under applicable law.


### Team Member Eligibility

To be eligible for FMLA Leave benefits, you must:

1. have worked for the Company for a total of at least 12 months;
1. have worked at least 1,250 hours over the previous 12 months as of the start of the leave; and
1. work at a location where at least 50 team members are employed by the Company within 75 miles, as of the date the leave is requested.

### Reasons for Leave

State and federal laws allow FMLA Leave for various reasons. Because a team member's rights and obligations may vary depending upon the reason for the FMLA Leave, it is important to identify the purpose or reason for the leave. FMLA Leave may be used for one of the following reasons, in addition to any reason covered by an applicable state family/medical
leave law:

1. the birth, adoption, or foster care of a team member's child within 12 months following birth or placement of the child ("Bonding Leave");
1.  to care for an immediate family member (spouse, child, or parent with a serious health condition ("Family Care Leave");
1. a team member's inability to work because of a serious health > condition ("Serious Health Condition Leave");
1. a "qualifying exigency," as defined under the FMLA, arising from a spouse's, child's, or parent's "covered active duty" (as defined below) as a member of the military reserves, National Guard or Armed Forces ("Military Emergency Leave"); or
1. to care for a spouse, child, parent or next of kin (nearest blood relative) who is a "Covered Servicemember," as defined below ("Military Caregiver Leave").

### Definitions

Child
: for purposes of **Bonding Leave and Family Care Leave** means a biological, adopted, or foster child, a stepchild, a legal ward, or a child of a person standing in loco parentis, who is either under age 18, or age 18 or older and incapable of self-care because of a mental or physical disability at the time that Family and Medical Leave is to commence.

Child
: for purposes of **Military Emergency Leave and Military Caregiver Leave** means a biological, adopted, or foster child, stepchild, legal ward, or a child for whom the person stood in loco parentis, and who is of any age.

Parent
: for purposes of this policy, means a biological, adoptive, step or foster father or mother, or any other individual who stood *in loco parentis* to the person. This term does not include parents "in law." For Military Emergency leave taken to provide care to a parent of a military member*, the parent must be incapable of self-care, as defined by the FMLA.*

Covered Active Duty
: means

    1. in the case of a member of a regular component of the Armed Forces, duty during the deployment of the member with the Armed Forces to a foreign country; and
    1. in the case of a member of a reserve component of the Armed Forces, duty during the deployment of the member with the Armed Forces to a foreign country under a call or order to active duty (or notification of an impending call or order to active duty) in support of a contingency operation as defined by applicable law.

Covered Servicemember
: means

    1. a member of the Armed Forces, including a member of a reserve component of the Armed Forces, who is undergoing medical treatment, recuperation, or therapy, is otherwise in outpatient status, or is otherwise on the temporary disability retired list, for a serious injury or illness incurred or aggravated in the line of duty while on active duty that may render the individual medically unfit to perform his or her military duties, or
    1. a person who, during the five (5) years prior to the treatment necessitating the leave, served in the active military, Naval, or Air Service, and who was discharged or released therefrom under conditions other than dishonorable (a "veteran" as defined by the Department of Veteran Affairs), and who has a qualifying injury or illness incurred or aggravated in the line of duty while on active duty that manifested itself before or after the member became a veteran. For purposes of determining the five-year period for covered veteran status, the period between October 28, 2009 and March 8, 2013 is excluded.

#### Length of Leave

The maximum amount of FMLA Leave will be twelve (12) workweeks in any 12-month period when the leave is taken for:

1. Bonding Leave;
1. Family Care Leave;
1. Serious Health Condition Leave; and/or
1.  Military Emergency Leave. However, if both spouses work for the Company and are eligible for leave under this policy, the spouses will be limited to a total of 12 workweeks off between the two of them when the leave is for Bonding Leave or to care for a parent using Family Care Leave. In determining eligibility for leave, a "rolling" twelve-month period is used, measuring forward from the date the team member uses any FMLA leave.

The maximum amount of FMLA Leave for a team member wishing to take Military Caregiver Leave will be a combined leave total of twenty-six (26) workweeks in a single 12-month period. A "single 12-month period" begins on the date of your first use of such leave and ends 12 months after that date.

If both spouses work for the Company and are eligible for leave under this policy, the spouses will be limited to a total of 26 workweeks off between the two when the leave is for Military Caregiver Leave only or is for a combination of Military Caregiver Leave, Military Emergency Leave, Bonding Leave and/or Family Care Leave taken to care for a parent.

Under some circumstances, you may take FMLA Leave intermittently -- which means taking leave in blocks of time, or by reducing your normal weekly or daily work schedule. Leave taken intermittently may be taken in increments of no less than one hour. Team members who take leave intermittently or on a reduced work schedule basis for planned medical treatment must make a reasonable effort to schedule the leave so as not to unduly disrupt the Company's operations. Please contact the People & Culture (HR) team prior to scheduling planned medical treatment. If Family and Medical Leave is taken intermittently or on a reduced schedule basis due to foreseeable planned medical treatment, the Company may require you to transfer temporarily to an available alternative position with an equivalent pay rate and benefits, including a part-time position, to better accommodate recurring periods of leave.  

When a team member who has been approved for intermittent leave seeks leave time that is unforeseeable, the team member must specifically reference either the qualifying reason for leave or the need for FMLA leave at the time the team member calls off.

As discussed more generally below, if your request for intermittent leave is approved, the Company may later require you to obtain recertifications of your need for leave. For example, the Company may request recertification if it receives information that casts doubt on your report that an absence qualifies for Family and Medical Leave.  


#### Notice and Certification

##### Bonding, Family Care, Serious Health Condition, and Military Caregiver Leave Requirements

Team members are required to provide:

- when the need for the leave is foreseeable, 30 days advance notice or such notice as is both possible and practical if the leave must begin in less than 30 days (normally this would be the same day the team member becomes aware of the need for leave or the next business day); 
- when the need for leave is not foreseeable, notice within the time prescribed by the Company's normal absence reporting policy, unless unusual circumstances prevent compliance, in which case notice is required as soon as is otherwise possible and practical; 
- when the leave relates to medical issues, a completed Certification of Health-Care Provider form within 15 calendar days (for Military Caregiver Leave, an invitational travel order or invitational travel authorization may be submitted in lieu of a Certification of Health-Care Provider form); 
- periodic recertification (upon request); and 
- periodic reports during the leave.

Certification forms are available from People & Culture (HR). At the Company's expense, the Company may also require a second or third medical opinion regarding your own serious health condition or the serious health condition of your family member. In some cases, the Company may require a second or third opinion regarding the injury or illness of a "Covered Servicemember." Team members are expected to cooperate with the Company in obtaining additional medical opinions that the Company may require.

When leave is for planned medical treatment, you must try to schedule treatment so as not to unduly disrupt the Company's operation. Please contact People & Culture (HR) prior to scheduling planned medical treatment.

#### Recertifications After Grant of Leave

In addition to the requirements listed above, if your Family and Medical Leave is certified, the Company may later require medical recertification in connection with an absence that you report as qualifying for Family and Medical Leave. For example, the Company may request recertification if

1. the team member requests an extension of leave;
1. the circumstances of the team member's condition as described by the previous certification change significantly (e.g., your absences deviate from the duration or frequency set forth in the previous certification; your condition becomes more severe than indicated in the original certification; you encounter complications); or
1. the Company receives information that casts doubt upon your stated reason for the absence. In addition, the Company may request recertification in connection with an absence after six months have passed since your original certification, regardless of the estimated duration of the serious health condition necessitating the need for leave. Any recertification requested by the Company shall be at the team member's expense.

#### Military Emergency Leave Requirements

Team members are required to provide:

- as much advance notice as is reasonable and practicable under the circumstances; 
- a copy of the covered military member's active duty orders when the team member requests leave and/or documentation (such as Rest and Recuperation leave orders) issued by the military setting forth the dates of the military member's leave; and 
- a completed Certification of Qualifying Exigency form within 15 calendar days, unless unusual circumstances exist to justify providing the form at a later date.

Certification forms are available from People & Culture (HR).

#### Failure to Provide Certification and to Return from Leave

Absent unusual circumstances, failure to comply with these notice and certification requirements may result in a delay or denial of the leave.  If you fail to return to work at your leave's expiration and have not obtained an extension of the leave, the Company may presume that you do not plan to return to work and have voluntarily terminated your employment.

#### Pay and Benefits During Leave

People & Culture (HR) will provide team members with a written explanation of the status of their pay and benefits at the start of the leave.

Generally, FMLA Leave is unpaid. However, you may be eligible to receive benefits through state-sponsored or Company-sponsored wage-replacement benefit programs. If you are eligible to receive these benefits, you may also choose to supplement these benefits with the use of accrued PTO, to the extent permitted by law and Company policy. All such payments will be integrated so that you will receive no more than your regular compensation during this period. If you are not eligible to receive any of these wage-replacement benefits, you will be required to apply accrued PTO. The use of paid benefits will not extend the length of an FMLA Leave.

- a.  Available PTO will be applied toward any FMLA-qualified leave and will run concurrently. After exhaustion of such PTO, remaining FMLA will be unpaid.  
- b.  While on FMLA leave, team members will be required to continue paying their portion of insurance premiums. If the FMLA leave is covered by paid leave, premiums will be deducted as usual. If the FMLA leave is unpaid, the team member must remit payment at the first of the month. If the team member fails to make the required payments, the insurance may be cancelled and the team member will remain responsible for the amount of the team member's share paid by the Company. The Company will take action to recover such monies.  
- c.  PTO time will not accrue during a FMLA leave and a team member will not be eligible for holiday pay or paid bereavement leave during a FMLA leave.  
- d.  Holidays occurring during a full week of FMLA leave count as FMLA leave; if a team member works any part of a work week during which a holiday falls, the holiday does not count as FMLA.  
- e.  Deductions for any hours of unpaid intermittent or reduced schedule FMLA leave may be made from the salaries of exempt or nonexempt team members after accrued PTO is exhausted.  
- f.  Team members who qualify for short term disability (STD) will receive pay in accordance with the terms of the plans.  
- g.  Bonuses or other payments based on achievement of a specified goal or based on days or hours actually worked or total earnings may take into account the absences or pro-rated based on FMLA absences if an equivalent non-FMLA leave is treated the same.  
- h.  Team members who qualify for worker's compensation benefits will receive pay continuation according to the requirements of state law and the Company's insurance plan in each state.  
- i.  While on FMLA leave, team members may not engage in other employment or work of any kind.

##### Benefits During Leave

The Company will continue making contributions for your group health benefits during your leave on the same terms as if you had continued to work. This means that if you want your benefits coverage to continue during your leave, you must also continue to make any premium payments that you are now required to make for yourself or your dependents. Team members taking Bonding Leave, Family Care Leave, Serious Health Condition Leave, and Military Emergency Leave will generally be provided with group health benefits for a 12 workweek period. Team members taking Military Caregiver Leave may be eligible to receive group health benefits coverage for up to a maximum of 26 workweeks. In some instances, the Company may recover premiums it paid to maintain health coverage if you fail to return to work following a FMLA Leave.


#### Job Reinstatement

Under most circumstances, you will be reinstated to the same position held at the time of the leave or to an equivalent position with equivalent pay, benefits, and other employment terms and conditions.  However, you have no greater right to reinstatement than if you had been continuously employed rather than on leave. For example, if you would have been laid off had you not gone on leave, or if your position has been eliminated during the leave, then you will not be entitled to reinstatement.

Prior to being allowed to return to work, a team member wishing to return from a Serious Health Condition Leave must submit an acceptable release from a health care provider that certifies the team member can perform the essential functions of the job as those essential functions relate to the team member's serious health condition. For a team member on intermittent FMLA leave, such a release may be required if reasonable safety concerns exist regarding the team member's ability to perform his or her duties, based on the serious health condition for which the team member took the intermittent leave.  

"Key team members," as defined by law, may be subject to reinstatement limitations in some circumstances. If you are a "key team member," you will be notified of the possible limitations on reinstatement at the time you request a leave.

#### Fraudulent Use of FMLA Prohibited

A team member who fraudulently obtains Family and Medical Leave from the Company is not protected by FMLA's job restoration or maintenance of health benefits provisions. In addition, the Company will take all available appropriate disciplinary action against such team member due to such fraud.

#### Additional Information Regarding FMLA

A Notice to Team Members Of Rights Under FMLA (WHD Publication 1420) is located at Appendix One.

## Leave of Absence

There may be a rare occasion when a team member is faced with an emergency or special circumstance and needs to take an unpaid personal leave of absence. The team member's manager, in conjunction with the Chief People Officer, may grant a personal leave of absence without pay.  

### Medical Leave (Non-FMLA)

Team members who do not qualify under FMLA but have a personal medical condition that prohibits them from being at work may qualify for unpaid medical leave. The Company will comply with all its obligations under the Americans With Disabilities Act ("ADA") and comparable state laws. 

Guidelines:

- Each request for a leave of absence will be evaluated on an individual basis, taking into consideration staffing and team needs, reason for the leave, the team member's length of service and work record.  
- Medical documentation must be provided. Requests for leaves of absences should be in writing and unless there is an emergency situation, all requests for leave should be made thirty (30) days prior to the start of the leave.  
- This leave can be taken only after all available PTO has been exhausted.  
- If the condition qualifies for short term disability, the leave will follow the duration of the approved STD leave with no additional leave being provided.  
- If a team member is on a personal leave of absence and becomes eligible for FMLA, the team member will no longer be eligible for a personal leave of absence if the leave falls under the provisions of FMLA.  
- While on leave, team members must continue to pay their portion of insurance premiums. If the leave is unpaid, premium payments are due the first (1) day of the month to the HR team.  
- PTO time will not accrue during a continuous leave and the team member will not be eligible for holiday pay or bereavement pay during the leave.  
- Failure to return from leave will constitute a resignation by the team member.

### Personal Leave

Personal leave of up to thirty (30) days may be extended for circumstances outside of the team member's own personal medical condition such as an immediate family member's illness or injury, other catastrophic personal matters that need immediate attention or other special circumstances.

Guidelines:

- Each request for a leave of absence will be evaluated on an individual basis, taking into consideration staffing and team needs, reason for the leave, the team member's length of service and work record.  
- Requests for leaves of absences should be in writing and unless there is an emergency situation, all requests for leave should be made thirty (30) days prior to the start of the leave.  
- To qualify for a personal leave of absence, a team member must be classified as full-time and must have completed at least six (6) months of full-time service at the time of the request.  
- This leave can be taken only after all available PTO has been exhausted.  
- Personal leaves of absence will only be granted one (1) time in a "rolling" backwards twelve (12) month period.  
- If a team member becomes eligible for FMLA while on a personal leave of absence, the leave will be converted to and becomes eligible for FMLA, the team member will no longer be eligible for a personal leave of absence if the leave falls under the provisions of FMLA.  
- While on leave, team members must continue to pay their portion of insurance premiums. If the leave is unpaid, premium payments are due the first (1) day of the month to the P&C (HR) team.  
- PTO time will not accrue during the leave and the team member will not be eligible for holiday pay or bereavement pay during the leave.  
- The Company cannot guarantee team members that their original position or equivalent position will be available when they return.  
- Failure to return from leave will constitute a resignation by the team member.

## Maternity Leave

Active, regular, full-time team members are eligible for six (6) weeks of paid maternity leave that must be taken during the pregnancy or immediately after the birth of their own baby. Maternity leave benefits will be concurrent with all leave policies, and not in addition to, any FMLA, Personal Leave or other leave that may be available. Maternity leave will be paid at 100% of base salary and the average of last twelve (12) months commissions in conjunction with Short Term Disability.  

**Note to Team members: Where a team member's applicable state law provides greater benefits than those described above, the state law will apply.**

Contact People & Culture (HR) for more information or questions about maternity leave.

## Parental Leave

All active, regular, full-time team members are eligible for up to 2 weeks of paid parental leave during the first three (3) months of the birth or adoption of their own child. Team members will be paid at 100% of their base salary and the average of last twelve (12) months commissions for the duration of the parental leave.

Parental leave benefits will be concurrent with all leave policies. If a team member is eligible for Parental Leave benefits, it will be offered concurrently with, not in addition to, any FMLA, Personal Leave or other leave that may be available. Parental Leave may be used either as 2 weeks of continuous leave or in 1 week increments.

To be eligible for Parental Leave benefits, a team member must notify his or her manager and the People & Culture (HR) Team for approval.  Requests for Parental Leave should be at least 30 days of the requested leave.

**Note to Team members: Where a team member's applicable state law provides greater benefits than those described above, the state law will apply.**

## Military Leave 

The company is committed to protecting the job rights of a team member absent on military leave. In accordance with the [Uniformed Services Employment and Reemployment Rights Act (USERRA)](https://www.dol.gov/agencies/vets/programs/userra), it is the Company's policy that no team member or prospective team member will be subjected to any form of discrimination on the basis of that person's membership in or obligation to perform service for any of the Uniformed Services of the United States. Specifically, no person will be denied employment, reemployment, promotion, or other benefit of employment on the basis of such membership. Furthermore, no person will be subjected to retaliation or adverse employment action because such person has exercised his or her rights under this policy.  

A military leave of absence will be granted by the Company to team members who are absent from work because of service in the United States uniformed services. Advance notice of military service, as far in advance as reasonable, is required unless military necessity prevents such notice or it is otherwise impossible or unreasonable. Subject to certain exceptions under the applicable laws, these benefits are generally limited to five (5) years of leave of absence. Contact the People & Culture (HR) Team to request a temporary or extended military leave of absence.

### Benefits

1.  Team members on temporary or extended military leave may, at their option, use any or all accrued paid time off during their absence.  
1.  Per USERRA, the company will maintain health benefits and other benefits for the first 31 days of military leave as if the team member is actively employed. After 31 days, the team member and dependents can continue group health coverage through COBRA at 102% of the overall premium rate.  
1.  Team members do not accrue PTO (paid time off), receive holiday or bereavement pay while on military leave of absence status.  
1.  Company paid benefits will terminate the day the team member becomes active military for more than 31 days.

#### Reemployment

Team members returning from military leave will be placed in the position they would have attained had they remained continually employed or a comparable one depending on the length of military service in accordance with USERRA. They will be treated as though they were continuously employed for purposes of determining benefits based on length of service.

Team members will be required to provide to the Company with military discharge documentation to establish the timeliness of the request for reemployment, the duration of the military service, and the honorable discharge from the military service.  

Upon a team member's prompt application for reemployment (as defined below), a team member will be reinstated to employment in the following manner depending upon the team member's period of military service:

1.  Less than 91 days of military service - (i) in a position that the team member would have attained if employment had not been interrupted by military service; or (ii) if found not qualified for such position after reasonable efforts by the Company to qualify the team member, in the position in which the team member has been employed prior to military service. If the team member is not qualified to perform either of these positions, the team member shall be reemployed in any position for which the team member is qualified in nearest approximation to first to the position the team member would have obtained, then the pre-service position, with full seniority.  
1.  More than 90 days and less than 5 years of military service - (i) in a position that the team member would have attained if employment had not been interrupted by military service or a position of like seniority, status and pay, the duties of which the team member is qualified to perform; or (ii) if proved not qualified after reasonable efforts by the Company to qualify the team member, in the position of like seniority, status and pay, the duties of which the team member is qualified to perform. If the team member is not qualified to perform either of these positions, the team member shall be reemployed in any position for which the team member is qualified in nearest approximation to first to the position the team member would have obtained, then the pre-service position, with full seniority.  
1.  Team member with a service-connected disability - if after reasonable accommodation efforts by the employer, a team member with a service connected disability is not qualified for employment in the position he or she would have attained or in the position that he or she left, the team member will be employed in
    - (i) any other position of similar seniority, status and pay for which the team member is qualified or could become qualified with reasonable efforts by the Company; or
    - (ii) if no such position exists, in the nearest approximation consistent with the circumstances of the team member's situation.  

A team member who has engaged in military service must, in order to receive the reemployment rights set forth above, submit a request for reemployment according to the following schedule:

1.  If service is less than 31 days (or for the purpose of taking an examination to determine fitness for service) - the team member must report for reemployment at the beginning of the first full regularly scheduled working period on the first calendar day following the completion of service and the expiration of eight hours to allow for safe transportation back to the team member's residence.  
1.  If service is 31 days or more but less than 181 days - the team member must submit a request for reemployment with People & Culture (HR) no later than 14 days following the completion of service.  
1.  If service is over 180 days - the team member must submit a request for reemployment with People & Culture (HR) no later than 90 days following completion of service.  

# EMPLOYMENT PRACTICES

## Equal Employment Opportunity

In order to provide equal employment and advancement opportunities to all individuals, employment decisions at ConstructConnect will be based on qualifications, abilities and merit. Equal employment opportunity is not only good practice - it's the law and applies to all areas of employment, including recruitment, selection, hiring, training, transfer, promotion and demotion, layoff and recall, termination, compensation and benefits.

As an equal opportunity employer, the Company does not discriminate in its employment decisions on the basis of race, religion, color, national origin, sex or pregnancy, age, disability, military or veteran status or any other basis that would be in violation of any applicable federal, state or local law. Furthermore, the Company will make reasonable accommodations for qualified individuals with known disabilities unless doing so would result in an undue hardship.

Any individuals with questions or concerns about any type of discrimination in the workplace should bring these issues to the attention of their immediate manager or the Chief People Officer.  Individuals can raise concerns and make reports without fear of reprisal. Anyone found to be engaging in any type of unlawful discrimination will be subject to disciplinary action, up to and including termination of employment.

## The American with Disabilities Act (ADA)

[The American with Disabilities Act (ADA)](https://www.ada.gov/) requires employers to reasonably accommodate qualified individuals with disabilities. It is the policy of the Company to comply with all Federal and state laws concerning the employment of persons with disabilities.

It is Company policy not to discriminate against qualified individuals with disabilities in regard to application procedures, hiring, advancement, discharge, compensation, training, or other terms, conditions, and privileges of employment.

The Company will reasonably accommodate qualified individuals with a temporary or long-term disability so that they can perform the essential functions of a job.

An individual who can be reasonably accommodated for a job, without undue hardship, will be given the same consideration for that position as any other applicant.

People & Culture (HR) is responsible for implementing this policy, including resolution of reasonable accommodation, safety, and undue hardship issues. Disabled team members who want reasonable accommodation should contact People & Culture (HR).

## Workplace Harassment

ConstructConnect strives to maintain a workplace that fosters mutual team member respect and promotes harmonious, productive working relationships. Our Company believes that discrimination and/or harassment in any form constitutes misconduct that undermines the integrity of the employment relationship. Therefore, the Company prohibits discrimination and/or harassment that is sexual, racial or religious in nature or is related to anyone's gender, national origin, age, sexual orientation, disability, veteran's or military status, or any other characteristic protected by applicable law. This policy applies to all team members throughout the Company and all individuals who may have contact with any team member of this Company.

The term "harassment" means any unwelcome verbal, visual, or physical conduct, comments, communications or treatment of a discriminatory nature about, relating to or because of a person's sex, race, color, religion, national origin, age or disability or other legally-protected characteristic or activity, which has the purpose or effect of unduly interfering with an individual's work performance; creates an intimidating, hostile, or offensive work environment; or otherwise adversely affects an individual's employment opportunities.

Examples of harassment include, but are not limited to, making inappropriate or offensive jokes or remarks relating to sex, race, color, religion, national origin, age or disability; using e-mail, voice mail, playing music or other methods of communication to disseminate such jokes or remarks; accessing such offensive material using Company equipment; distributing such jokes or remarks received from others outside the Company.

The term "sexual harassment" means unwelcome sexual advances, requests for sexual favors, and/or verbal or physical conduct of a sexual nature.

Examples of sexual harassment include but are not limited to drawings, pictures, accessing offensive material, sexually suggestive humor or behavior including staring or ogling, unacceptable voicemails/e-mails, uninvited and unnecessary physical contact, and other sexually related comments and behavior.

Unwelcome sexual advances, requests for sexual favors, or other verbal, visual or physical conduct of a harassing nature will also constitute harassment when a person involved feels compelled to submit to that misconduct in order to keep their position, to receive appropriate pay, or to benefit from certain employment decisions. If this type of misconduct interferes with a team member's work or creates an intimidating, hostile or offensive work environment, it may also be considered harassment.

If a team member feels they have been subjected to any form of harassment and/or discrimination, the team member should firmly and clearly tell the person engaging in the harassing and/or discriminating conduct that it is unwelcome, offensive and should stop at once. The team member also should report any discrimination and/or harassment to the Chief People Officer. At that point they will take the necessary steps to initiate an investigation of the discrimination and/or harassment claim.

People & Culture (HR) will conduct an investigation in as confidential a manner as possible and as permitted by law. A timely resolution of each complaint will be reached and communicated to the team member and the other parties involved. Appropriate disciplinary action, up to and including termination, will be taken promptly against any team member engaging in discrimination and/or harassment. Retaliation against any team member for filing a complaint or participating in an investigation is strictly prohibited. However, any team member that knowingly makes a false claim of harassment and/or discrimination will be subject to disciplinary action up to and including termination.

Occasionally, talking with a manager or someone in People & Culture (HR) about this conduct is not an option. If a team member feels that his/her complaint has not been or cannot be properly handled, he/she may forward the complaint to Chief Operating Officer.

# WORKPLACE SAFETY AND SECURITY

## Visitors in the Workplace 

In order to assure the safety and security of Company team members, security of sensitive/proprietary/confidential information, its visitors, and to ensure that only authorized personnel have access to the Company facilities, all visitors on Company property must enter the building through the lobby.  All visitors are required to sign the Visitors log and must wear and clearly display a visitor's badge while on the premises at all times where applicable. Visitors shall not be permitted outside the lobby without being escorted at all times by an appropriate team member.  Team members are responsible for the conduct, locality and well-being of their visitors at all times.  It is the affirmative obligation of each team member to report to management any strangers in the workplace that look like they don't belong there and/or are apparently unattended.  Team members must have their visitors surrender their visitor's badge when they are leaving the building and sign out on the Visitors log.

## Computer and Internet Usage 

The term "Company Computer Systems" in context of this policy includes Company-provided internet, instant messaging, common drives, e-mail, phone systems, and all related hardware and software. This policy applies to all types of communication activities utilizing Company owned computers, communication equipment, systems and networks.  

### Team Member Usage Expectations

Access to the Company's IT resources is a privilege granted to all users
to support their job duties and to support the goals and objectives of
the Company.

The Company expects that team members will:

- Login to the system using the identification and authentication provided to them (unique user ID and password) which may be changed by the team member at a later date.  
- Operate computer equipment for the performance of their official duties only during working time. The use of computer equipment for personal or non-business purposes should be limited to non-working time, and inappropriate use may result in disciplinary actions against the team member, as deemed appropriate by management.  
- Conduct themselves honestly and appropriately while using computer equipment, and respect the copyrights, software licensing rules, property rights and privacy of others.  
- Take proper care of all company equipment that the team member is entrusted with. Upon leaving the Company a team member will return all company equipment in proper working order. Failure to return equipment will be considered theft and will lead to criminal prosecution by the Company.

The Company requires that team members will not:

- Attempt to disable, defeat or circumvent any Company security feature or mechanism.  
- Access, archive, store, distribute, edit, print, display, or record any kind of sexually explicit, threatening, demeaning, harassing, offensive or otherwise inappropriate software or electronic files (images, documents, videos, etc.).  
- Attempt without prior authorization to alter the content, structure or functionality of Company-supplied facilities, i.e., content of the web site, sequence of presentation, routing and/or linking to other sites or addresses, etc.  
- Attaching personal devices to Company equipment (e.g. USB drives, personal music players, personal computers, laptops and/or games) is prohibited.  
- Use Company resources to operate a privately-owned business, solicit funds, or promote religious beliefs to others.  
- Intentionally or carelessly perform an act that will place an excessive load on a computer or network to the extent that other users may experience denied or disrupted service.  
- Use the Company's logo, trademark or proprietary graphics for any commercial purpose, without authorization, or in a manner that suggests you are representing the Company, or while engaging in activity that is unlawful or violates Company policy.  
- Downloading or storing confidential Company or customer information is prohibited.  
- Downloading any software or electronic files without implementing virus protection measures that have been approved by the Company.  
- Knowingly violate the laws and regulations of the United States or any other nation, or the laws and regulations of any state, city, or other local jurisdiction.  

*Use of any of these resources for illegal activity may be grounds for
discipline up to and including immediate termination.*

### Internet Access

Anyone identified or identifiable as a team member is considered a representative of the Company and must behave accordingly, including while on multi-media, social networking, blogs and wiki sites for both professional and personal use. The following guidelines should be followed.

- Team members inadvertently connected to an Internet site that contains offensive or sexually explicit material, must disconnect from that site immediately and notify their manager.  
- Team members may access social media sites for personal use during break periods and lunch breaks only unless directly related to a job requirement per the Company computer usage policy.  
- If being on social media sites is part of your job responsibilities, then you must clearly identify yourself as an ConstructConnect team member in your postings or blog site(s) and include a disclaimer that the views are your own and not those of ConstructConnect unless you are authorized in writing by your manager to do so. You are not permitted to post comments in the name of the employer or in a manner that could reasonably be attributed to the employer, without prior written authorization.  
- ConstructConnect team members should not circulate postings they know are written by other ConstructConnect team members without informing the recipient that the author of the posting is a ConstructConnect team member.  
- Because you are legally responsible for your postings, you may be subject to liability if your posts are found defamatory, harassing, threatening, intimidating, bullying, consist of discriminatory comments, slanderous comments, maliciously false comments, or in violation of any other applicable law.  
- Your Internet postings should respect copyright, privacy, fair use, financial disclosure, and other applicable laws. Your postings should not contain proprietary, trade secret or attorney-client privileged information.  
- You may also be liable if you make postings which include proprietary or copyrighted information (music, videos, text, etc.) belonging to third parties. All of the above-mentioned postings are prohibited under this policy.  
- Team members with Internet access may not use Company resources to download and/or play entertainment software, music or games.  
- Downloading streaming video (e.g., watching movies through the Internet) or to participate in newsgroups with automatic update features (e.g., Pod cast) is not permitted, unless the use is directly related to a job requirement.

- While at work, creating and responding to blogs, wikis, and other
  forms of personal online discourse must be restricted to work-related requirements.  
- All the Company rules and policies regarding confidentiality and proprietary information apply to cyberspace commentary or blog activities. Team members are personally responsible and liable for the content of information posted to cyberspace commentary or blogs.  
- Information communicated on any social media site that violates the Company policies or constitutes harassment of co-workers; even during off hour activities and/or on a third-party site is a serious offense and will be subject to disciplinary action, up to and including termination of employment.

### E-Mail Management and Instant Messaging (IM)

- Use of Company e-mail is for business purposes and limited personal usage only.  E-mail correspondence is not private, and the Company can and will access and read any and all information contained in computers, computer files, e-mail messages or voice messages.  Team members should have no expectation of privacy with regard to these communications and will be in violation of the Company's discrimination and harassment policy if they send, receive or access discriminatory, harassing or otherwise inappropriate e-mails or voice mails. The content of e-mail messages is admissible as evidence and may be subject to subpoena in discovery.  
- Do not send or forward e-mails that contain libelous, defamatory, offensive, harassing, explicit, racist, or obscene remarks.  
- Do not send e-mails to entire office or Company without first obtaining written authorization from the Chief People Officer.  
- Do not send work containing confidential client information from an office computer to a home computer via e-mail.  Company-related e-mail must not be stored or transferred to non-work-related computers.  
- Instant messaging is to be used for business purposes only, and it is expected that users will communicate professionally at all times.  The Company can and will monitor, inspect, copy, review, store and audit IM usage.  
- Transmitting confidential information through IM is forbidden.  Credit card numbers are not to be transmitted or stored via email or IM/chat.

### Harassment

Anyone receiving any kind of sexually explicit, threatening, demeaning, harassing, offensive or otherwise inappropriate image on any Company computer system or electronic communications system should retain the document, correspondence or file and immediately notify their manager.  

### Passwords and Locking/Logging Off

- The responsibility for protecting authentication credentials to systems and applications falls upon the individual to whom those provisions were originally provided. Sharing of user accounts is prohibited. Employees should never give their password to anyone.  The user ID and password combination is confidential and therefore: 
  - Must not be shared with any other person, including administrators and supervisors. This includes passwords to non-guest Wi-Fi.  
  - Must not be written down on any material so that another individual can access it. Periodic walkthroughs should be performed to ensure compliance.  
  - Must not be stored in an electronic format on the computer or email unless properly restricted through encryption or secured password vault.  
- Users are expected to lock their computers when walking away from their office or desk. This includes workstations, laptops, mobile devices and servers. Although settings may be enabled to enforce automatic lockout and timeouts, the stronger control resides with users locking their devices as soon as they step away. Users are responsible for actions taken using their device(s), including those performed by the adversary.  
  - Press the Windows Key + L to lock your Windows Computer 
  - Press Ctrl + Shift + Eject/Power to lock your Mac Computer 
- When leaving work at night or over the weekend "Log off". Do not use the "Shut Down" option as periodic maintenance is sometimes performed, and computers must be powered on to receive these important updates.

### User Privacy

Users should have no expectation of privacy when using information systems and mobile devices above what is required by local laws and regulations. To manage systems and enforce security, the organization may log, review and otherwise utilize any information stored on or passing through its systems. User activity may be captured such as telephone numbers dialed and websites visited.

Messages sent over computer and communications systems are the property of the company. Management reserves the right to examine all data stored in or transmitted by these systems.

The users should seek assistance from the IT Security personnel to ensure the security of the transmission channel. When providing computer-networking services, the company may not necessarily provide default message protection services, such as encryption. No responsibility is assumed for the disclosure of information sent over networks and no assurances are made about the privacy of information handled by internal networks. In those instances where session encryption or other special controls are required, it is the user's responsibility to ensure that adequate security precautions are taken

### Reporting Security Events

All workers should promptly report to IT any loss of, or severe damage to, their hardware or software. Workers should report all suspected compromises as well as all serious security vulnerabilities known to exist. All instances of suspected disclosure of confidential information also should be reported.

- To report incidents you can enter a ticket in the Sysaid ticket system, send an email to <servicedelivery@constructconnect.com> or contact the Service Desk at 888-202-4490 

### Cloud Storage & File Transfers

Cloud storage solutions and file transfer software (i.e., Dropbox, box.com, etc.) are not allowed for storage or transmission of any confidential company data unless the method is approved by the local security team, as listed below. Storage of confidential data in unsecure locations can represent a significant exposure risk to the organization 

### Data Sensitivity

Customer data and other proprietary information on all Company computers, networks and databases are owned by the Company and is a confidential asset governed by Federal, State and Local laws and confidentiality rules and regulations. Team members are expected to value and respect the confidential and sensitivity of this information and data. Any violations will be grounds for immediate termination.  

### Data Classification

Data is classified into three basic categories: Public, Internal Use Only and Confidential. If information is not marked with one of these categories, it will default into the Internal Use Only category. If you have questions about which category a document should fall in please speak with your manager or contact the IT Security team by submitting a Sysaid ticket with information on the document in question.  

- Public Data: Information in the public domain, which are approved for public use (i.e., annual reports, marketing brochures, press statements etc.). Security at this level is minimal and unauthorized access would not cause problems for the company, its customers and business partners. No special precautions should be taken to protect data classified as public data and it can be distributed to anyone.  
- Internal Use Data: This information is intended for use within the company and, in some cases, within affiliated organizations, such as business partners. Unauthorized disclosure of this information to outsiders may be against laws and regulations, or may cause problems for the company, its customers or its business partners. This type of information is already widely distributed within the company or it could be distributed within the organization. Examples are the company telephone book, organizational charts, cost center codes, internal system names and most internal electronic mail messages.  Although the data may not appear dangerous, skilled attackers can use this information to obtain confidential data much easier.  
- Confidential Data: This information is private and most sensitive in nature and should be restricted to those with a legitimate business need for access. Unauthorized disclosure of this information to people without a business need for access may be against laws and regulations or may cause significant problems for the company, its customers or its business partners. Decisions about the provision of access to this information should be cleared through the information Owner. Examples are non-public financial data, trade secrets, proprietary source code, customer transaction, personally identifiable information (PII), Protected Health Information (PHI), Payment Card Industry (PCI) data, merger and acquisition plans legal information protected by attorney-client privilege and worker performance evaluation records.

### Remote and Wireless Access

- Approved users may access the Company network remotely through the use of a virtual private network (VPN) connection. This access can be approved by the user's team manager and a ticket should be submitted via SysAid for access.  
- Log off remote sessions immediately after completing work and do not leave computers unattended while remote sessions are active.  
- When wireless access is established via an externally-supplied access point (AP), the team member is responsible for applying the highest level of security available for that AP. Confidential data is not to be sent/received over any wireless connections without strong encryption.  

Because of the evolving nature of technology, this policy is not intended to be comprehensive, and conduct not specifically mentioned that violates the spirit of this policy may also, at our discretion, result in corrective action up to and including termination.

Violations of any part of this policy can lead to corrective action up to and including termination. The Company will cooperate with legal authorities in investigations of any potential illegal violations regarding e-mail and/or Internet and/or cyberspace use.

### Social Media 

At the Company, we understand that social media can be a fun and rewarding way to share your life and opinions with family, friends and co-workers around the world. However, use of social media also presents certain risks and carries with it certain responsibilities. To assist you in making responsible decisions about your use of social media, the Company has established these guidelines for appropriate use of social media.

In the rapidly expanding world of electronic communications, social media can mean many things. Social media includes all means of communicating or posting information or content of any sort on the Internet, including to your own or someone else's web log or blog, journal or diary, personal web site, social networking and affinity web site, web bulletin board or chat room, whether or not associated or affiliated with the Company, as well as any other form of electronic communication.

The same principles and guidelines found in all our Company policies apply to your activities online. Ultimately, you are solely responsible for what you post online. Before creating online content, consider some of the risks and rewards that are involved. Keep in mind that any of your conduct that adversely affects your job performance, the performance of fellow team members or otherwise adversely affects clients or people who work on behalf of the Company or the Company's legitimate business interests may result in disciplinary action up to and including termination.

Know and follow the rules
: Carefully read these guidelines, the Company's policy on use of Company Electronic Resources and the Company's policy prohibiting unlawful discrimination & harassment, and ensure your postings are consistent with these policies. Postings that include discriminatory remarks, harassment, and threats of violence or similar inappropriate or unlawful conduct will not be tolerated and may subject you to disciplinary action up to and including termination.

Be Respectful
: Always be fair and courteous to customers, vendors and suppliers. Also, keep in mind that you are more likely to resolve work-related complaints by speaking directly with your co-workers or by utilizing our Open-Door Policy than by posting complaints to a social media outlet.  Nevertheless, if you decide to post complaints or criticism, avoid using statements, photographs, video or audio that reasonably could be viewed as malicious, obscene, threatening or intimidating, that disparage customers, vendors, suppliers or members of the public, or that might constitute harassment or bullying. Examples of such conduct might include offensive posts meant to intentionally harm someone's reputation or posts that could contribute to a hostile work environment on the basis of race, sex, disability, religion or any other status protected by law or Company policy.

Avoid Posting Information You Know to be False
: Always strive to be honest and accurate when posting information or news, and if you make a mistake, correct it quickly. Be open about any previous posts you have altered. Remember that the Internet archives almost everything; therefore, even deleted postings can be searched.  Never post any information or rumors that you know to be false about the Company, fellow associates, customers, vendors, suppliers or members of the public.  

Maintain confidentiality
: Maintain the confidentiality of Confidential Information, as that term is described elsewhere in this Handbook, and do not disclose non-public customer, vendor or supplier information. Do not create a link from your blog, website or other social networking site to a Company website without identifying yourself as a Company team member.

Express only your personal opinions
: Express only your personal opinions. Never represent yourself as a spokesperson for the Company. If the Company is a subject of the content you are creating, be clear and open about the fact that you are a team member and make it clear that your views do not represent those of the Company, fellow associates, members, clients, suppliers or people working on behalf of the Company. If you do publish a blog or post online related to the work you do or subjects associated with the Company, make it clear that you are not speaking on behalf of the Company. It is best to include a disclaimer such as "The postings on this site are my own and do not necessarily reflect the views of the Company."

Using social media at work
: Refrain from using social media while on working time unless it is work-related as authorized by your manager. Do not use the Company email addresses to register on social networks, blogs or other online tools utilized for personal use.

Retaliation is prohibited
: The Company prohibits taking negative action against any team member for reporting a possible deviation from this policy or for cooperating in an investigation. Any team member who retaliates against another team member for reporting a possible deviation from this policy or for cooperating in an investigation will be subject to disciplinary action, up to and including termination.

## Confidential & Proprietary Information

Team members of ConstructConnect will receive and have access to information that is confidential in nature to the Company, its customers and vendors. It is the responsibility and the legal obligation of all team members to safeguard confidential Company information. The economic well-being of our Company depends upon protecting and maintaining proprietary Company information. Additionally, we are responsible for the security of confidential client information. Confidential information includes but is not limited to trade secrets or confidential information relating to products, processes, know-how, debtors, clients, customers, designs, drawings, test data, marketing data, business plans and strategies, and negotiations and contracts.

Notwithstanding the foregoing, "Confidential Information" does not include information lawfully acquired by non-management team members about wages, hours or other terms and conditions of employment if used by them for purposes protected under SS7 of the National Labor Relations Act such as joining or forming a union, engaging in collective bargaining, or engaging in other concerted activity for their mutual aid or protection. Nothing in this policy prohibits team members from filing a charge or complaint with, or from participating in, an investigation or proceeding conducted by the NLRB, or from exercising rights under Section 7 of the NLRA to engage in joint activity with other team members.

To ensure that we hold ourselves accountable and conduct ourselves in a professional manner, ConstructConnect and its affiliates will adhere to the spirit, as well as the intent, of all state, federal and local laws and regulations that apply to our industry. Team members will receive training on those laws and regulations that are applicable to their job.  

The Company has developed certain proprietary products and processes that are unique to the Company. Keeping such information from competitors plays an important part in our success. The Company protects proprietary information by restricting team members and visitor's access to certain designated areas and access to documents to only those who have business reasons to view them.

Team members who improperly use or disclose trade secrets or confidential business information will be subject to disciplinary action, up to and including termination of employment and legal action, even if they do not actually benefit from the disclosed information.

All media containing confidential information will be physically secured. Any internal or external distribution of confidential data will be strictly controlled. If confidential media is sent offsite, it will be sent via delivery method that can be tracked and approved by management.

All media containing CST data will be labeled as "Confidential". When that data is no longer needed, it will be destroyed by shredding, incineration or pulped.

Nothing in this policy prohibits you from reporting an event that you reasonably and in good faith believe is a violation of law to the relevant law-enforcement agency, or from cooperating in an investigation conducted by such a government agency. This may include disclosure of trade secret or confidential information within the limitations permitted by the Defend Trade Secrets Act (DTSA). You are notified that under the DTSA, no individual will be held criminally or civilly liable under Federal or State trade secret law for disclosure of a trade secret (as defined in the Economic Espionage Act) that is: (A) made in confidence to a Federal, State, or local government official, either directly or indirectly, or to an attorney, and made solely for the purpose of reporting or investigating a suspected violation of law; or, (B) made in a complaint or other document filed in a lawsuit or other proceeding, if such filing is made under seal so that it is not made public. And, an individual who pursues a lawsuit for retaliation by an employer for reporting a suspected violation of the law may disclose the trade secret to the attorney of the individual and use the trade secret information in the court proceeding, if the individual files any document containing the trade secret under seal, and does not disclose the trade secret, except as permitted by court order.

## Confidential Information Data Protection 

The protection of confidential business information and trade secrets is vital to the interests and success of the Company. Such confidential information includes, but is not limited to, the following examples: 

- Financial account numbers, such as credit card and bank numbers 
- Debtor, client and/or customer information of any kind 
- Pending projects and proposals

All team members are required to sign a confidentiality agreement as a condition of employment.

Access to systems will be restricted to only include those individuals that have a job need.

Team members who improperly use or disclose confidential business information will be subject to disciplinary action, including termination of employment and legal action, even if they do not actually benefit from the disclosed information.

## Inclement Weather 

The offices of ConstructConnect and its subsidiaries rarely close due to inclement weather conditions.  It is the Company's practice to review the need to close a location based on the specific locality and/or city plan or declaration.

It is recognized that severe weather can make it difficult and sometimes dangerous to commute to work.  Team members are urged to use sound judgment and consider individual circumstances in determining whether or not to attempt to commute to work.  If a team member is unable to report to work or will be reporting late, they should notify their immediate manager prior to the start of their shift. Team members unable to make it to work when the office is open will not be issued disciplinary action for absences caused by inclement weather but PTO will be used for time missed.

The management team in each office will make the decision to close an office due to inclement weather conditions.

## Safety in the Workplace

It is the policy of ConstructConnect that every team member is entitled to work under the safest possible conditions. To this end, every reasonable effort will be made in the interest of accident prevention, fire protection, and health preservation.

Each team member and manager must practice safety awareness by thinking defensively, anticipating unsafe situations and reporting unsafe conditions immediately.

Please observe the following precautions:

- Notify a manager of any unsafe condition or emergency situation. If an illness or injury occurs at work inform your manager or People & Culture (HR) immediately. Call 911, do not attempt to move the individual or transport the individual to the hospital on your own.  
- The use of alcoholic or controlled substances, or the abuse of legal prescription drugs during work hours will not be tolerated.  
- Use, adjust, and repair machines and equipment only when trained and qualified to do so.  
- Know the locations, contents and use of first aid and firefighting equipment.

A violation of safety standards, which cause hazardous or dangerous situations, or team members who fail to report or, where appropriate, remedy such situations, may be subject to disciplinary action, up to and including termination of employment.

## Workplace Violence

The safety and security of all team members is of primary importance at ConstructConnect. Threats, threatening and abusive behavior, or acts of violence against team members, visitors, customers, vendors or other individuals by anyone will not be tolerated. Violations of this policy will lead to accelerated disciplinary action, not corrective action, up to and including termination and/or referral to appropriate law enforcement agencies for arrest and prosecution. The Company can and will take any necessary legal action to protect its team members, customers and property.

Any person who makes threats, exhibits threatening behavior or engages in violent acts on Company premises shall be removed from the premises as quickly as safety permits and shall remain off Company premises pending the outcome of the investigation. Following investigation, the Company will initiate an immediate and appropriate response. This response may include, but is not limited to, suspension and/or termination of any business relationship, reassignment of job duties, suspension or termination of employment and/or criminal prosecution of the person or persons involved.

All team members are responsible for notifying management of any threats that they witness or receive or that they are told another person witnessed or received. Even without a specific threat, all team members should report any behavior that they have witnessed that they regard as potentially threatening or violent or which could endanger the health or safety of a team member when the behavior has been carried out on a Company-controlled site or is connected to Company employment or Company business. Team members are responsible for making this report regardless of the relationship between the individual who initiated the threatening behavior and the person or persons being threatened. The Company understands the sensitivity of the information requested and has developed confidentiality procedures in accordance with applicable law that recognize and respect the privacy of the reporting team member.

## Weapons-Free Workplace

To ensure that ConstructConnect maintains a workplace safe and free of violence for all team members, the Company prohibits the possession or use of weapons of any kind on Company property. A license to carry the weapon does not supersede Company policy. Any team member in violation of this policy will be subject to prompt disciplinary action, up to and including termination. All Company team members are subject to this provision, including contract and temporary team members, visitors, vendors and customers on Company property.

Dangerous weapons
: include, but are not limited to, firearms, explosives, chemical irritants, stun guns, clubs, illegal knives and other weapon or device that might be considered dangerous or that could cause harm. Team members are responsible for making sure that any item possessed by the team member is not prohibited by this policy.

Company property
: is defined as all Company-owned or leased buildings and surrounding areas such as sidewalks, walkways, driveways and parking lots under the Company's ownership or control. This policy applies to all vehicles that come onto Company property where permitted by applicable law.

Team members are also prohibited from carrying or possessing a weapon while in the course and scope of performing their job for the Company, whether they are on Company property at the time or not and whether they are licensed to carry a handgun or not as permitted by applicable law.  Team members may not possess or carry any weapon or other device covered by this policy while performing any task on the Company's behalf. This policy also prohibits weapons at any Company sponsored function such as parties or picnics, or in any team member vehicle that the team member uses in the course and scope of the team member's employment.

The Company reserves the right at any time and at its discretion to search vehicles, packages, containers, briefcases, purses, lockers, desks, enclosures and persons entering its property, for the purpose of determining whether any weapon has been brought onto its property or premises in violation of this policy. Team members that refuse to promptly permit a search under this policy will be subject to discipline up to and including termination.

The only exception to this policy are police officers, security guards or other persons who have been given written consent by the Company to carry a weapon on the property.

This policy is administered and enforced by People & Culture (HR).  Anyone with questions or concerns specific to this policy should contact the Chief People Officer.

## Right to Search/Privacy Expectations 

Access to ConstructConnect premises is conditioned upon its right to inspect or search the person, vehicle or personal effects of any team member or visitor. This may include any team member's office, desk, computer & related equipment, file cabinet, closet, locker, lunchbox, clothing or similar place. Team members should have no expectation of privacy in connection with any of these listed places. Because even a routine inspection or search might result in the viewing of a team member's personal possessions, team members are encouraged not to bring any item of personal property to the workplace that they do not want revealed to others in the Company.

Any prohibited materials (or materials that may be found to be prohibited) that are found in a team members possession during an inspection or search will be collected and retained by management and placed in a sealed container or envelope. The team member's name, date, circumstances under which the materials were collected, and by whom they were collected will be recorded and attached to the container or written upon the envelope. If after further investigation, the collected materials prove not to be prohibited, they will be returned to the team member, and the team member must sign a receipt for the contents. If the prohibited materials prove to be illegal and/or dangerous, they will not be returned to the team member but may be turned over to the appropriate authorities.

From time to time, and without prior announcement, inspections or searches may be made of anyone entering, leaving, or on the premises or property of the Company (including alcohol and/or drug screens or other testing). Refusal to cooperate in such an inspection or search (including alcohol and/or drug screens) shall be grounds for disciplinary action, up to and including termination.

# SEPARATION OF EMPLOYMENT

## Resignation 

Resignation is a voluntary act initiated by the team member to terminate employment with the Company. A team member resigning is requested to give at least two (2) weeks' notice before voluntarily terminating employment so that a smooth transition may occur in filling their vacated position. After a team member tenders their resignation they are no longer able to take PTO hours and floating holidays.

Team members that work out their two (2) week notice will be paid out unused, accrued PTO time. Team members that do not work out their two (2) week notice will not be paid out unused, accrued PTO time where permitted by applicable law.

Team members that have their employment involuntarily terminated because of non-compliance with any local, state or federal law or violation of the Company's policies will not be paid out unused, accrued PTO time where permitted by applicable law.

Management reserves the right to provide a team member with two weeks' pay in lieu of notice in situations where job or business needs warrant.

Team members who fail to report to work for three consecutive days without properly communicating to their manager the reasons for their absences will be viewed as voluntarily resigning their employment as of the third day.

Team members are required to turn in all Company property in proper working order no later than their last day of work. A team member not returning Company property will not be considered eligible for re-hire.  Failure to return company equipment will be considered theft and the Company may take legal action against the team member to recover monies representative of the property not returned.

When team members leave the Company, they may be asked to participate in an exit interview. The primary purpose of the exit interview is to ask for valuable feedback about team members' work experiences at the Company. Participation in such exit interviews is strictly voluntary.

Any team member that has had their employment terminated from the Company is not permitted to be on Company property without the advance permission of the Chief People Officer. Contact with team members during work hours is also prohibited. Any violations to this policy may result in legal action.

# APPENDIX ONE

## A Notice to Team Members of Rights Under the FMLA

[FMLA Poster (pdf format)](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/fmlaen.pdf)
