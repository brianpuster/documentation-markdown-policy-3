Documentation Markdown Policy
-----------------------------

Documentation displayed by the Policydocs_ server, located at https://policydocs.buildone.co/pages/policy.

Documentation is written in ``markdown`` format and must be written in plain text.

All documentation begins with a ``yaml``-style header (also called "metadata"), followed by two empty lines. The header must contain the following for your document to be displayed:

::

    title: Sample Policy
    published: 2019-11-01
    author: Joe Doakes
    
    
Note the ``published`` value must be in ``YYYY-MM-DD`` format.

Updating Docs
=============
The simplest way to update a document, is to directly edit it in Bitbucket, by simply opening the ``.md`` file you wish to change, then clicking the ``[Edit]`` button.

Creating Docs
=============

#. Click on a folder in ``ConstructConnect/CCFinance/finance-documentation-markdown`` on Bitbucket.
#. Click the elipsis menu (``...``) next to the ``[Check out]`` button in the top right of the page.
#. Choose "Add file"
#. Add a new file name in the ``filename`` text box, making sure the filename's extesion is ``.md``
#. Add the ``title``, ``published`` and ``author`` key/value pairs, followed by two empty lines.
#. Add your markdown using the instructions in the Markdown_Demo_Page_
#. Save the document.

Publishing Docs
===============
#. Browse to the Policydocs_ server
#. After logging in, click the [Pull Updates] link in the menu.

If you do not see a ``[Pull Updates]`` link in the menu, contact doug.shawhan@constructconnect.com for ``publish`` access.

Gotchas
=======

Format Metadata Headers Properly
________________________________
Make sure that the header portion of your document contains the required newlines. The preview may show the header entries all on the same line, which is probably fine, as long as each entry is on it's own line in edit mode. If you don't have your entries on their own line, it will either not display, or may cause the service to behave badly.

Creating Documents by Pasting from MS Word
___________________________________________
If you are given a document in ``word`` format, it may contain characters that cause problems for the markdown interpreter. When logged in to the documentation service, you will see a menu choice called [``Convert .docx to .md``], which allows you to upload a ``.docx`` file. Copy-and-past this output into your new markdown document on Bitbucket. Don't forget to format your headers properly!


Notes
=====
This ``README`` file is not written in ``markdown``, rather it uses an older, more complicated standard called ``ReStructured Text (rst)``. This was done so the ``README`` appears nicely formatted in Bitbucket, but does not appear on the documentation service.


.. _Policydocs: https://policydocs.buildone.co/
.. _Markdown_Demo_Page: https://policydocs.buildone.co/docs/Meta/Markdown%20Instructions/Markdown_Demo_Page

